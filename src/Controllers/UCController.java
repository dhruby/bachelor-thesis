/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package Controllers;

import TabbedPanel.UCConnectManager;
import ToolBar.useCase.Line;
import java.awt.Point;
import ToolBar.useCase.MainObject;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/*
 * Controler pro diagram uziti
*/
public class UCController extends Controller {

    private UCConnectManager connectManager = new UCConnectManager();

    /*
     * Funkce pro pridani spojovaci hrany
    */
    public void addLine(Line l) {
        l = getCenteredLine(l);
        l.setStartObject(getObjectByCoordinates(l.getStartPoint()));
        l.setEndObject(getObjectByCoordinates(l.getEndPoint()));
        
        if (l.getStartObject() != l.getEndObject() && connectManager.canBeConnected(l.getStartObject(), l.getEndObject(), l.getType())) {
            l.setStartCoordinates();
            l.setEndCoordinates();
            lineList.add(l);
        }

    }

    /*
     * Funkce pro zobrazeni formulare objektu
    */
    public void displayForm(Point clickedPoint) {
        MainObject object = getObjectByCoordinates(clickedPoint);
        JTextField nameField = new JTextField(object.getLabelText());
        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.add(new JLabel("Name: "));
        panel.add(nameField);

        int result = JOptionPane.showConfirmDialog(null, panel, "Change name",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            object.setLabel(nameField.getText());
            if (object.getAssocObject() != null) {
                object.getAssocObject().setLabel(nameField.getText());
            }
        }
    }
}
