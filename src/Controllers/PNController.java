/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package Controllers;

import TabbedPanel.PNConnectManager;
import TabbedPanel.PNTab;
import ToolBar.OOPN.Place;
import ToolBar.OOPN.Transition;
import ToolBar.classDiagram.Attribute;
import ToolBar.useCase.Line;
import ToolBar.useCase.MainObject;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ToolBar.classDiagram.Class;
import ToolBar.classDiagram.Method;
import java.util.ArrayList;
/*
 * Controler pro zalozku Petriho site
 */
public class PNController extends Controller {

    private PNConnectManager connectManager;
    private PNTab tab;
    private MainObject selectedMethodItem;

    /*
     * Konstruktor
     */
    public PNController(PNTab tab) {
        connectManager = new PNConnectManager();
        this.tab = tab;
    }

    /*
     * Funkce pro pridani hrany
     */
    public void addLine(Line l) {
        l = getCenteredLine(l);

        l.setStartObject(getObjectByCoordinates(l.getStartPoint()));
        l.setEndObject(getObjectByCoordinates(l.getEndPoint()));

        if (connectManager.canBeConnected(l.getStartObject(), l.getEndObject())) {
            l.setStartCoordinates();
            l.setEndCoordinates();

            if (isInObjectList(l.getStartObject()) && isInObjectList(l.getEndObject())) {
                lineList.add(l);
            } else {
                addObjectToMethodPlace(l);
            }
        }
    }

    /*
     * Funkce pro zjisteni, zdali se jedna o objekt v siti tridy nebo metody
     */
    private boolean isInObjectList(MainObject object) {
        for (MainObject o : objectList) {
            if (o == object) {
                return true;
            }
        }

        for (MainObject o : lineList) {
            if (o == object) {
                return true;
            }
        }
        return false;
    }

    /*
     * Pridani objektu do site metody
     */
    public void addObjectToMethodPlace(MainObject object) {
        tab.getClassItem().addObjectToMethodPlace(tab.getSelectedMethod(), object);
    }

    /*
     * Funkce pro ziskani objektu podle souradnic
     */
    public MainObject getObjectByCoordinates(Point p) {
        for (MainObject object : objectList) {
            if (object.getBounds().contains(p)) {
                return object;
            }
            if (object instanceof Place) {
                for (MainObject pnObject : ((Place) object).getPnList()) {
                    if (new Rectangle(pnObject.getX() + (tab.getDrawingBoardWidth() / 2), pnObject.getY(), pnObject.getWidth(), pnObject.getHeight()).getBounds().contains(p)) {
                        return pnObject;
                    }
                }

                for (MainObject l : ((Place) object).getPnLineList()) {
                    if (l.contains(p)) {
                        return l;
                    }
                }
            }
        }

        for (MainObject object : lineList) {
            if (object.contains(p)) {
                return object;
            }
        }
        return null;
    }

    /*
     * Funkce volana pri kliknuti na objekt
     * podle objektu se zavola prislusny formular
     */
    public void clickedOnObject(Point p) {
        MainObject object = getObjectByCoordinates(p);
        if (object instanceof Transition) {
            displayTransitionForm((Transition) object);
        } else if (object instanceof Place) {
            displayPlaceForm((Place) object);
        } else if (object instanceof Line) {
            displayLineForm((Line) object);
        }

    }

    /*
     * Funkce pro zobrazeni formulare mista
     */
    public void displayPlaceForm(Place p) {
        JTextField nameField = new JTextField(p.getLabelText());
        JPanel panel = new JPanel(new GridLayout(0, 1));
        JButton ok = new JButton("OK");

        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent) e.getSource());
                pane.setValue(ok);
            }
        });

        panel.add(new JLabel("Name: "));
        panel.add(nameField);

        Object[] options = {ok, "Cancel"};
        int result = JOptionPane.showOptionDialog(null, panel, "Action form",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, ok);

        if (result == 0) {
            p.setLabel(nameField.getText());
            if (p.getAssocObject() != null) {
                if (p.getAssocObject() instanceof Method) {
                    if (!nameField.getText().equals("")) {
                        Method m = (Method) p.getAssocObject();
                        if (m.getName().equals(tab.getSelectedMethod())) {
                            m.setName(nameField.getText());
                            tab.setMethodForm(nameField.getText());
                        } else {
                            m.setName(nameField.getText());
                        }
                    } else {
                        JOptionPane.showMessageDialog(tab, "Method cannot be empty string!", "Warning!", JOptionPane.WARNING_MESSAGE);
                        displayPlaceForm(p);
                    }
                } else if (p.getAssocObject() instanceof Attribute) {
                    if (!nameField.getText().equals("")) {
                        Attribute a = (Attribute) p.getAssocObject();
                        a.setName(nameField.getText());
                    } else {
                        JOptionPane.showMessageDialog(tab, "Attribute cannot be empty string!", "Warning!", JOptionPane.WARNING_MESSAGE);
                        displayPlaceForm(p);
                    }
                }

            }
        }
    }

    /*
     * Funkce pro zobrazeni formulare spojovaci hrany
     */
    public void displayLineForm(Line l) {
        JTextField nameField = new JTextField(l.getLabelText());
        JPanel panel = new JPanel(new GridLayout(0, 1));
        JButton ok = new JButton("OK");

        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent) e.getSource());
                pane.setValue(ok);
            }
        });

        panel.add(new JLabel("Description: "));
        panel.add(nameField);

        Object[] options = {ok, "Cancel"};
        int result = JOptionPane.showOptionDialog(null, panel, "Action form",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, ok);

        if (result == 0) {
            l.setLabel(nameField.getText());
        }
    }

    /*
     * Funkce pro zobrazeni formulare prechodu
     */
    public void displayTransitionForm(Transition t) {
        JTextField descriptionField = new JTextField(t.getLabelText());
        JTextField guardField = new JTextField(t.getGuard());
        JTextField actionField = new JTextField(t.getAction());
        JPanel panel = new JPanel(new GridLayout(0, 1));
        JButton ok = new JButton("OK");

        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent) e.getSource());
                pane.setValue(ok);
            }
        });

        panel.add(new JLabel("Description:"));
        panel.add(descriptionField);
        panel.add(new JLabel("Guard: "));
        panel.add(guardField);
        panel.add(new JLabel("Action: "));
        panel.add(actionField);

        Object[] options = {ok, "Cancel"};
        int result = JOptionPane.showOptionDialog(null, panel, "Action form",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, ok);

        if (result == 0) {
            t.setLabel(descriptionField.getText());
            t.setGuard(guardField.getText());
            t.setAction(actionField.getText());
        }
    }

    protected JOptionPane getOptionPane(JComponent parent) {
        JOptionPane pane = null;
        if (!(parent instanceof JOptionPane)) {
            pane = getOptionPane((JComponent) parent.getParent());
        } else {
            pane = (JOptionPane) parent;
        }
        return pane;
    }

    /*
     * Prepsana metoda z predka. Funkce pro posun objektu
     */
    @Override
    public void moveObjects(MainObject object, Point p) {
        if (isInObjectList(object) && (p.x + object.getWidth() / 2) < (tab.getDrawingBoardWidth() / 2)
                || !isInObjectList(object) && (p.x - object.getWidth() / 2) > (tab.getDrawingBoardWidth() / 2)) {

            p.x -= object.getWidth() / 2;
            p.y -= object.getHeight() / 2;

            object.setCoordinates(p);

            if (isInObjectList(object)) {
                for (Line l : lineList) {
                    if (l.getStartObject() == object) {
                        l.setStartPoint(object.getCenteredPoint());
                        //l.setEndPoint(object.getCenteredPoint());
                        l.setCoordinates();
                    }

                    if (l.getEndObject() == object) {
                        //l.setStartPoint(object.getCenteredPoint());
                        l.setEndPoint(object.getCenteredPoint());
                        l.setCoordinates();
                    }
                }
            } else {
                for (MainObject o : objectList) {
                    if (o instanceof Place) {
                        for (Line l : ((Place) o).getPnLineList()) {
                            if (l.getStartObject() == object) {
                                Point centeredPoint = object.getCenteredPoint();
                                centeredPoint.x += tab.getDrawingBoardWidth()/2;
                                l.setStartPoint(centeredPoint);
                                //l.setEndPoint(object.getCenteredPoint());
                                l.setCoordinates();
                            }

                            if (l.getEndObject() == object) {
                                Point centeredPoint = object.getCenteredPoint();
                                centeredPoint.x += tab.getDrawingBoardWidth()/2;
                                l.setEndPoint(centeredPoint);
                                l.setCoordinates();
                            }
                        }
                    }
                }
            }
        }
    }

    /*
     * Funkce pro vymazani vybraneho objektu
     */
    public void deleteSelectedItem(Class c, String selectedMethod) {
        if (selectedItem != null || selectedMethodItem != null) {
            if (selectedItem != null) {
                if (selectedItem instanceof Line) {
                    lineList.remove(selectedItem);
                } else {
                    deleteItemLines(selectedItem);
                    objectList.remove(selectedItem);
                    if (selectedItem instanceof Place && isMethod(c) && selectedItem.getAssocObject() != null) {
                        c.getMethodList().remove(selectedItem.getAssocObject());
                    }
                    if (selectedItem instanceof Place && isAttribute(c) && selectedItem.getAssocObject() != null) {
                        c.getAttributeList().remove(selectedItem.getAssocObject());
                    }
                    c.computeHeight();
                }
            } else {
                Place p = getPlaceByMethodName(selectedMethod, c);
                if (selectedMethodItem instanceof Line) {
                    p.getPnLineList().remove(selectedMethodItem);
                } else {
                    deleteItemLines(selectedMethodItem, p.getPnLineList());
                    p.getPnList().remove(selectedMethodItem);
                }
            }
        }
        selectedItem = null;
        selectedMethodItem = null;
    }

    /*
     * Funkce pro ziskani mista podle nazvu metody
     */
    private Place getPlaceByMethodName(String methodName, Class c) {
        for (MainObject o : c.getPnList()) {
            if (o instanceof Place && o.getLabelText().equals(methodName)) {
                return (Place) o;
            }
        }
        return null;
    }

    /*
     * Funkce pro vymazani pridruzenych spojovacich hran
     */
    protected void deleteItemLines(MainObject object, ArrayList<Line> lineList) {
        ArrayList<Line> removableLines = new ArrayList<>();
        for (Line l : lineList) {
            if (l.getStartObject() == object) {
                removableLines.add(l);
            } else if (l.getEndObject() == object) {
                removableLines.add(l);
            }
        }

        lineList.removeAll(removableLines);
    }

    /*
     * Funkce pro zjisteni, zdali sejedna o metodu
     */
    private boolean isMethod(Class c) {
        for (Method m : c.getMethodList()) {
            if (m.getName() == selectedItem.getLabelText()) {
                return true;
            }
        }
        return false;
    }

    /*
     * Funkce pro odstraneni metody
     */
    private void removeMethod(Class c) {
        for (Method m : c.getMethodList()) {
            if (m.getName() == selectedItem.getLabelText()) {
                c.removeMethod(m);
                return;
            }
        }
    }

    /*
     * Funkce pro zjisteni, zdali se jedna o atribut
     */
    private boolean isAttribute(Class c) {
        for (Attribute a : c.getAttributeList()) {
            if (a.getName() == selectedItem.getLabelText()) {
                return true;
            }
        }
        return false;
    }

    /*
     * Funkce pro odstraneni atributu
     */
    private void removeAttribute(Class c) {
        for (Attribute a : c.getAttributeList()) {
            if (a.getName() == selectedItem.getLabelText()) {
                c.removeAttribute(a);
                return;
            }
        }
    }

    /*
     * Funkce pro vraceni vybraneho objektu metody
     */
    public MainObject getSelectedMethodItem() {
        return selectedMethodItem;
    }

    /*
     * Funkce pro nastaveni vybraneho prvku metody
     */
    public void setSelectedMethodItem(MainObject selectedMethodItem) {
        this.selectedMethodItem = selectedMethodItem;
    }
}
