/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package Controllers;

import ToolBar.OOPN.Place;
import ToolBar.useCase.Line;
import ToolBar.useCase.MainObject;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;

abstract class Controller {

    // Promenna pro ulozeni objektu v diagramu
    protected ArrayList<MainObject> objectList = new ArrayList<>();

    // Promenna pro ulozeni spojovacich hran 
    protected ArrayList<Line> lineList = new ArrayList<>();
    protected MainObject movingObject;
    protected MainObject selectedItem;
    protected final int margin = 5;

    public ArrayList<MainObject> getObjectList() {
        return objectList;
    }

    public ArrayList<Line> getLineList() {
        return lineList;
    }

    public MainObject getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(MainObject selectedItem) {
        this.selectedItem = selectedItem;
    }

    public void addObject(MainObject object) {
        objectList.add(object);
    }

    /*
     * Funkce pro vymazani vybraneho objektu
     */
    public void deleteSelectedItem() {
        if (selectedItem != null) {
            if (selectedItem instanceof Line) {
                lineList.remove(selectedItem);
            } else {
                // vymazou se i vsechny hrany, ktere vedou od/do objektu
                deleteItemLines(selectedItem);
                // nastavi se asociovany objekt pro vymazani
                if (selectedItem.getAssocObject() != null) {
                    selectedItem.getAssocObject().setDelete(true);
                }
                objectList.remove(selectedItem);
            }
            selectedItem = null;
        }
    }

    /*
     * Funkce pro vymazani objektu
     */
    public void deleteItem(MainObject o) {
        // vymazou se i vsechny hrany, ktere vedou od/do objektu
        deleteItemLines(o);
        objectList.remove(o);
    }

    /*
     * Funkce pro vymazání přidružených hran
     */
    protected void deleteItemLines(MainObject object) {
        ArrayList<Line> removableLines = new ArrayList<>();
        for (Line l : lineList) {
            if (l.getStartObject() == object) {
                removableLines.add(l);
            } else if (l.getEndObject() == object) {
                removableLines.add(l);
            }
        }

        lineList.removeAll(removableLines);
    }

    /*
     * Pridani spojovaci hrany k diagramu
     */
    public void addLine(Line l) {
        l = getCenteredLine(l);
        l.setStartObject(getObjectByCoordinates(l.getStartPoint()));
        l.setEndObject(getObjectByCoordinates(l.getEndPoint()));

        if (l.getStartObject() != l.getEndObject()) {
            l.setStartCoordinates();
            l.setEndCoordinates();
            lineList.add(l);
        }

    }

    /*
     *  Funkce pro vycentrovani spojovaci hrany
     */
    protected Line getCenteredLine(Line line) {
        Point startPoint = new Point((int) line.getStartX(), (int) line.getStartY());
        Point endPoint = new Point((int) line.getEndX(), (int) line.getEndY());

        startPoint = getCenteredPoint(startPoint);
        endPoint = getCenteredPoint(endPoint);
        line.setStartPoint(startPoint);
        line.setEndPoint(endPoint);

        return line;
    }

    /*
     * Funkce pro ziskani stredoveho bodu objektu
     */
    private Point getCenteredPoint(Point p) {
        return getObjectByCoordinates(p).getCenteredPoint();
    }

    /*
     * Funkce pro získani objektu podle souradnic
     */
    public MainObject getObjectByCoordinates(Point p) {
        for (MainObject object : objectList) {
            if (object.getBounds().contains(p)) {
                return object;
            }
        }

        for (MainObject object : lineList) {
            if (object.contains(p)) {
                return object;
            }
        }
        return null;
    }

    /*
     * Funkce pro presouvani objektu
     */
    public void moveObjects(MainObject object, Point p) {

        p.x -= object.getWidth() / 2;
        p.y -= object.getHeight() / 2;

        object.setCoordinates(p);

        for (Line l : lineList) {
            if (l.getStartObject() == object) {
                l.setStartPoint(object.getCenteredPoint());
                //l.setEndPoint(object.getCenteredPoint());
                l.setCoordinates();
            }

            if (l.getEndObject() == object) {
                //l.setStartPoint(object.getCenteredPoint());
                l.setEndPoint(object.getCenteredPoint());
                l.setCoordinates();
            }
        }
    }

    /*
     * Funkce pro zvyrazneni objektu
     */
    public void highlightItem(Graphics g, MainObject object) {
        g.setColor(Color.RED);
        g.drawRect(object.getX() - margin, object.getY() - margin, object.getWidth() + 2 * margin, object.getHeight() + 2 * margin);
    }

    /*
     * funkce pro zvyrazneni spojovaci hrany
     */
    public void highlightLine(Graphics g, MainObject object) {
        g.setColor(Color.RED);
        object.draw(g);
    }

    /*
     * Funkce pro nastaveni listu objektu pri importu
     */
    public void setObjectList(ArrayList<MainObject> objectList) {
        this.objectList = objectList;
    }

    /*
     * Funkce pro nastaveni spojovacich hran pri importu
    */
    public void setLineList(ArrayList<Line> lineList) {
        this.lineList = lineList;
    }

}
