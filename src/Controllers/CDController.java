/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package Controllers;

import TabbedPanel.CDConnectManager;
import ToolBar.classDiagram.Attribute;
import java.awt.Point;
import ToolBar.useCase.MainObject;
import java.awt.GridLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ToolBar.classDiagram.Class;
import ToolBar.classDiagram.Method;
import ToolBar.useCase.Line;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * Controller pro diagram trid
 */
public class CDController extends Controller {

    /*
     * trida pro kontrolu spojeni
     */
    private CDConnectManager connectManager;

    public CDController() {
        connectManager = new CDConnectManager();
    }

    /**
     * Funkce pri pridani spojovaci hrany k diagramu
     */
    public void addLine(Line l) {
        // prvne vycentruji
        l = getCenteredLine(l);
        // nasledne nastavim prislusne objekty
        l.setStartObject(getObjectByCoordinates(l.getStartPoint()));
        l.setEndObject(getObjectByCoordinates(l.getEndPoint()));

        // zjistim, jestli lze objekty spojit
        if (l.getStartObject() != l.getEndObject() && connectManager.canBeConnected(l.getStartObject(), l.getEndObject(), l.getType())) {
            // upravim hranu, aby sla k hrane objektu
            l.setStartCoordinates();
            l.setEndCoordinates();

            //pridam hranu
            lineList.add(l);
        }

    }

    /**
     * Funkce pro zobrazeni formulare pro pridani atributu
     */
    public void attributeClick(Point p) {
        MainObject object = getObjectByCoordinates(p);
        if (object != null) {
            displayAddAttribueForm((Class) object);
        }
    }

    /**
     * Funkce pro zobrazeni formulare pro pridani metody
     */
    public void methodClick(Point p) {
        MainObject object = getObjectByCoordinates(p);
        if (object != null) {
            displayAddMethodForm((Class) object);
        }
    }

    /**
     * Po kliknuti na objekt se rozhodne, jaky formular se ma zobrazit
     */
    public void clickedOnObject(Point p) {

        // Ziskam prislusnou tridu
        Class c = (Class) getObjectByCoordinates(p);

        // zobrazim prislusny formular
        if (c.isClickedOnHead(p)) {
            nameForm(p);
        } else if (c.isClickedOnAttribut(p)) {
            displayEditAttribueForm(c, c.getClickedAttribute(p));
        } else if (c.isClickedOnMethod(p)) {
            displayEditMethodForm(c, c.getClickedMethod(p));
        }
    }

    /*
     * Formular pro pridani atributu 
     */
    public void displayAddAttribueForm(Class c) {
        String accesses[] = {"+ : public", "- : private", "# : protected", "~ : package"};
        JComboBox accessComboBox = new JComboBox(accesses);
        JTextField nameField = new JTextField();
        JTextField dataTypeField = new JTextField();
        JPanel panel = new JPanel(new GridLayout(0, 1));
        JButton ok = new JButton("OK");
        ok.setEnabled(false);

        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent) e.getSource());
                pane.setValue(ok);
            }
        });

        // priradim prvky k panelu formulare
        panel.add(new JLabel("Attribute access: "));
        panel.add(accessComboBox);
        panel.add(new JLabel("Attribute name: "));
        panel.add(nameField);
        panel.add(new JLabel("Data type: "));
        panel.add(dataTypeField);

        // nastavim povinne pole
        setRequired(nameField, dataTypeField, ok);
        setRequired(dataTypeField, nameField, ok);

        Object[] options = {ok, "Cancel"};
        int result = JOptionPane.showOptionDialog(null, panel, "Add attribute",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, ok);

        // pokud jsem kliknul na ok, ulozim
        if (result == 0) {
            String name = nameField.getText();
            String dataType = dataTypeField.getText();

            c.addAttribute(new Attribute(accessComboBox.getSelectedItem().toString().substring(0, 1), name, dataType));

        }
    }

    protected JOptionPane getOptionPane(JComponent parent) {
        JOptionPane pane = null;
        if (!(parent instanceof JOptionPane)) {
            pane = getOptionPane((JComponent) parent.getParent());
        } else {
            pane = (JOptionPane) parent;
        }
        return pane;
    }

    /*
     * Nastaveni povinych poli 
     */
    public void setRequired(JTextField field, JTextField field2, JButton btn) {
        field.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                changed();
            }

            public void removeUpdate(DocumentEvent e) {
                changed();
            }

            public void insertUpdate(DocumentEvent e) {
                changed();
            }

            public void changed() {
                if (field.getText().equals("") || field2.getText().equals("")) {
                    btn.setEnabled(false);
                } else {
                    btn.setEnabled(true);
                }

            }
        });
    }

    /*
     * Nastaveni povinych poli 
     */
    public void setRequired(JTextField field, JButton btn) {
        field.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                changed();
            }

            public void removeUpdate(DocumentEvent e) {
                changed();
            }

            public void insertUpdate(DocumentEvent e) {
                changed();
            }

            public void changed() {
                if (field.getText().equals("")) {
                    btn.setEnabled(false);
                } else {
                    btn.setEnabled(true);
                }

            }
        });
    }

    /*
     * Zobrazeni formulare pro editaci atributu
     */
    public void displayEditAttribueForm(Class c, Attribute a) {
        String accesses[] = {"+ : public", "- : private", "# : protected", "~ : package"};
        JComboBox accessComboBox = new JComboBox(accesses);
        JTextField nameField = new JTextField(a.getName());
        JTextField dataTypeField = new JTextField(a.getDataType());
        JButton ok = new JButton("OK");

        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent) e.getSource());
                pane.setValue(ok);
            }
        });

        // nastaveni povinych poli
        setRequired(nameField, dataTypeField, ok);
        setRequired(dataTypeField, nameField, ok);

        // pridani prvku do panelu
        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.add(new JLabel("Attribute access: "));
        panel.add(accessComboBox);
        panel.add(new JLabel("Attribute name: "));
        panel.add(nameField);
        panel.add(new JLabel("Data type: "));
        panel.add(dataTypeField);

        Object[] options = {ok,
            "Cancel",
            "Delete"};

        int result = JOptionPane.showOptionDialog(null, panel, "Edit attribute",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
        // zpracovani kliknuteho tlacitka
        if (result == 0) {
            a.setAccess(accessComboBox.getSelectedItem().toString().substring(0, 1));
            a.setName(nameField.getText());
            a.setDataType(dataTypeField.getText());
            if (a.getAssocObject() != null) {
                a.getAssocObject().setLabel(nameField.getText());
            }
        } else if (result == 2) {
            c.removeAttribute(a);
        }
    }

    /*
     * Funkce pro zobrazeni komentare pro pridani medoty
     */
    public void displayAddMethodForm(Class c) {
        String accesses[] = {"+ : public", "- : private", "# : protected", "~ : package"};
        JComboBox accessComboBox = new JComboBox(accesses);
        JTextField nameField = new JTextField();
        JTextField dataTypeField = new JTextField();
        JButton ok = new JButton("OK");
        ok.setEnabled(false);

        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent) e.getSource());
                pane.setValue(ok);
            }
        });

        // nastaveni povineho pole
        setRequired(nameField, ok);

        // pridani prvku do panelu formulare
        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.add(new JLabel("Method access: "));
        panel.add(accessComboBox);
        panel.add(new JLabel("Method name: "));
        panel.add(nameField);
        panel.add(new JLabel("Return type: "));
        panel.add(dataTypeField);

        Object[] options = {ok, "Cancel"};
        int result = JOptionPane.showOptionDialog(null, panel, "Add method",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, ok);

        // zpracovani kliknuteho tlacitka
        if (result == 0) {
            String methodName = nameField.getText();
            String returnType = dataTypeField.getText();

            // pokud neni zadan navratovy typ, nastavi se na void
            if (returnType.equals("")) {
                returnType = "void";
            }
            c.addMethod(new Method(accessComboBox.getSelectedItem().toString().substring(0, 1), methodName, returnType));
        }
    }

    /*
     * Formular pro zobrazeni formulare pro editaci metody
    */
    public void displayEditMethodForm(Class c, Method m) {
        String accesses[] = {"+ : public", "- : private", "# : protected", "~ : package"};
        JComboBox accessComboBox = new JComboBox(accesses);
        JTextField nameField = new JTextField(m.getName());
        JTextField dataTypeField = new JTextField(m.getDataType());
        JButton ok = new JButton("OK");

        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent) e.getSource());
                pane.setValue(ok);
            }
        });

        // nastaveni povineho pole
        setRequired(nameField, ok);

        // pridani prvku k panelu formulare
        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.add(new JLabel("Method access: "));
        panel.add(accessComboBox);
        panel.add(new JLabel("Method name: "));
        panel.add(nameField);
        panel.add(new JLabel("Return type: "));
        panel.add(dataTypeField);

        Object[] options = {ok,
            "Cancel",
            "Delete"};

        int result = JOptionPane.showOptionDialog(null, panel, "Edit method",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
        
        // zpracovani kliknuteho tlacitka
        if (result == 0) {
            m.setAccess(accessComboBox.getSelectedItem().toString().substring(0, 1));
            m.setName(nameField.getText());
            String returnType = dataTypeField.getText();
            if (returnType.equals("")) {
                returnType = "void";
            }
            m.setDataType(returnType);
            if (m.getAssocObject() != null) {
                m.getAssocObject().setLabel(nameField.getText());
            }
        } else if (result == 2) {
            c.removeMethod(m);
        }
    }

    // formular pro zmenu labelu objektu
    public void nameForm(Point clickedPoint) {
        MainObject object = getObjectByCoordinates(clickedPoint);
        JTextField nameField = new JTextField(object.getLabelText());
        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.add(new JLabel("Name: "));
        panel.add(nameField);

        int result = JOptionPane.showConfirmDialog(null, panel, "Change name",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            if (!nameField.getText().equals("")) {
                object.setLabel(nameField.getText());
                // pokud existuje pridruzeny objekt, zmena se propise i k nemu
                if (object.getAssocObject() != null) {
                    object.getAssocObject().setLabel(nameField.getText());
                }
            } else {
                // Hlaska pro upozorneni uzivatele
                JOptionPane.showMessageDialog(null, "Class name cannot be empty string!", "Warning!", JOptionPane.WARNING_MESSAGE);
                nameForm(clickedPoint);
            }
        }
    }

}
