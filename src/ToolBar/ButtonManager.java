/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package ToolBar;

import ToolBar.useCase.Association;
import ToolBar.useCase.Extend;
import ToolBar.useCase.Generalization;
import ToolBar.useCase.Include;
import ToolBar.OOPN.ArcBrn;
import java.util.ArrayList;
import javax.swing.JButton;

/*
 * Trida slouzici pro prirazeni tlacitek k tabum
 */
public class ButtonManager {

    private ArrayList<JButton> list;
    private ToolBar parentToolBar;

    public ButtonManager(ToolBar parentToolBar) {
        list = new ArrayList<>();
        this.parentToolBar = parentToolBar;
    }

    /*
    * Funkce pro pridani tlacitek k tabu
     */
    public ArrayList<JButton> addUseCaseButtons() {
        list.add(new Button("Actor", "person.png", parentToolBar));
        list.add(new Button("Use case", "useCase.png", parentToolBar));
        list.add(new Association("Association", "association.png", parentToolBar));
        list.add(new Generalization("Generalization", "generalization.png", parentToolBar));
        list.add(new Include("Include", "include.png", parentToolBar));
        list.add(new Extend("Extend", "include.png", parentToolBar));

        return list;
    }

    /*
    * Funkce pro pridani tlacitek k tabu
     */
    public ArrayList<JButton> addClassDiagramButtons() {
        list.add(new Button("Class", "class.png", parentToolBar));
        list.add(new Button("Interface", "interface.png", parentToolBar));
        list.add(new Button("Attribute", "attribute.png", parentToolBar));
        list.add(new Button("Method", "method.png", parentToolBar));
        list.add(new Association("Association", "association.png", parentToolBar));
        list.add(new Generalization("Generalization", "generalization.png", parentToolBar));

        return list;
    }

    /*
    * Funkce pro pridani tlacitek k tabu
     */
    public ArrayList<JButton> addOOPNButtons() {
        list.add(new Button("Place", "place.png", parentToolBar));
        list.add(new Button("Transition", "transition.png", parentToolBar));
        list.add(new ArcBrn("Arc", "arc.png", parentToolBar));

        return list;
    }
}
