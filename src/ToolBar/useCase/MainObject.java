/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package ToolBar.useCase;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.UUID;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;

/*
 * Zakladni objekt pro prvky v aplikaci
 */
public class MainObject extends JComponent {

    protected UUID id;
    protected ImageIcon img = null;
    protected int x;
    protected int y;
    protected int width;
    protected int height;
    protected boolean isSelected = false;
    protected JLabel label;
    protected boolean isGeneratedAssocObject = false;
    protected MainObject assocObject = null;
    protected UUID assocObjectID = null;
    protected boolean delete = false;
    private final int ARR_SIZE = 4;

    /*
     * Zakladni konsturktor
     */
    public MainObject() {
        id = UUID.randomUUID();
        label = new JLabel("");
    }

    /*
     * Kosntruktor s ID, slouzi pro import
     */
    public MainObject(UUID id) {
        this.id = id;
    }

    /* 
     * Konstruktor se souradnicemi
     */
    public MainObject(int x, int y) {
        this.x = x;
        this.y = y;
        id = UUID.randomUUID();
    }

    /*
     * Funkce pro zjisteni, zdali bylo kliknuto na objekt
     */
    public boolean isClickOnObject(Point p) {
        if (new Rectangle(x, y, width, height).getBounds().contains(p)) {
            return true;
        }

        return false;
    }

    public void draw(Graphics g) {

    }

    /*
     * Funkce pro ziskani stredoveho bodu
     */
    public Point getCenteredPoint() {
        int centeredX = x;
        int centeredY = y;

        return new Point(centeredX, centeredY);
    }

    /*
     * Funkce pro nastaveni souradnic
     */
    public void setCoordinates(Point p) {
        x = p.x;
        y = p.y;
    }

    /*
     * Funkce pro nastaveni popisku
     */
    public void setLabel(String text) {
        label.setText(text);
    }

    /*
     * Funkce pro ziskani popisku
     */
    public String getLabelText() {
        return label.getText();
    }

    /*
     * Funkce pro ziskani obdelni okolo objektu
     */
    @Override
    public Rectangle getBounds() {
        return new Rectangle(x, y, width, height);
    }

    /*
     * Funkce pro ziskani sirky
     */
    @Override
    public int getWidth() {
        return width;
    }

    /*
     * Funkce pro ziskani vysky
     */
    @Override
    public int getHeight() {
        return height;
    }

    /*
     * Funkce pro ziskani x souradnice
     */
    public int getX() {
        return x;
    }

    /*
     * Funkce pro nastaveni x souradnice
     */
    public void setX(int x) {
        this.x = x;
    }

    /*
     * Funkce pro ziskani y souradnice
     */
    public int getY() {
        return y;
    }

    /*
     * Funkce pro nastaveni y souradnice
     */
    public void setY(int y) {
        this.y = y;
    }

    /*
     * Funkce pro ziskani asociovaneho objektu
     */
    public MainObject getAssocObject() {
        return assocObject;
    }

    /*
     * Funkce pro nastaveni asociovaneho objektu
     */
    public void setAssocObject(MainObject assocObject) {
        this.assocObject = assocObject;
    }

    /*
     * Funkce pro ziskani priznaku delete
     */
    public boolean getDelete() {
        return delete;
    }

    /*
     * Funkce pro nastaveni priznaku delete
     */
    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    /*
     * Funkce pro nastaveni sirky
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /*
     * Funkce pro nastaveni vysky
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /*
     * Funkce pro nastaveni popisu
     */
    public void setLabel(JLabel label) {
        this.label = label;
    }

    /*
     * Funkce pro zjisteni, zdali byl vygenerovat asociativni objekt
     */
    public boolean isIsGeneratedAssocObject() {
        return isGeneratedAssocObject;
    }

    /*
     * Funkce pro nstaveni priznaku
     */
    public void setIsGeneratedAssocObject(boolean isGeneratedAssocObject) {
        this.isGeneratedAssocObject = isGeneratedAssocObject;
    }

    /*
     * Funkce pri ziskani id objektu
     */
    public UUID getId() {
        return id;
    }

    /*
    * Funkce pro nastaveni id 
     */
    public void setId(UUID id) {
        this.id = id;
    }

    /*
     * Funkce pro ziskani id asociativniho objektu
     */
    public UUID getAssocObjectID() {
        return assocObjectID;
    }

    /*
     * Funkce pro nastaveni id asociativniho objektu
     */
    public void setAssocObjectID(UUID assocObjectID) {
        this.assocObjectID = assocObjectID;
    }

}
