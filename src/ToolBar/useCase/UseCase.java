/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package ToolBar.useCase;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.UUID;
import javax.swing.JLabel;

/*
 * Trida predstavujici pripad uziti v diagramu uziti
*/
public class UseCase extends MainObject {

    private int marginBottom = 20;
    private int margin = 5;
    private Ellipse2D ellipse;

    /*
     * Zakladni konstruktor
    */
    public UseCase(){
        label = new JLabel("");
    }
    
    /*
     * Konstruktor s ID pro import
    */
    public UseCase(UUID id){
        this.id = id;
        label = new JLabel("");
    }
    
    /*
     * Konstruktor se souradnicemi
    */
    public UseCase(int x, int y) {
        super(x, y);
        label = new JLabel("Use case");
        width = 200;
        height = 100;
        ellipse = new Ellipse2D.Double(x /*- (width / 2)*/, y /*- (height / 2)*/, width, height);
    }
    
    /*
     * Funkce pro vytvoreni elipsy pripadu uziti
    */
    public void createEllipse(){
        ellipse = new Ellipse2D.Double(x /*- (width / 2)*/, y /*- (height / 2)*/, width, height);
    }

    /*
     * Funkce pro zjisteni, zdali bylo kliknuto na objekt
    */
    public boolean isClickOnObject(Point p) {
        if (ellipse.getBounds().contains(p)) {
            return true;
        }
        return false;
    }

    /*
     * Funkce pro ziskani trojuhelniku okolo objektu
    */
    public Rectangle getBounds() {
        return new Rectangle(x , y , width, height).getBounds();
    }

    /*
     * Funkce pro vykresleni objektu
    */
    public void draw(Graphics g) {
        FontMetrics fm = g.getFontMetrics();
        width = fm.stringWidth(label.getText()) + margin * 10;

        Graphics2D g2 = (Graphics2D) g;
        g2.setPaint(new Color(237, 209, 123));
        ellipse.setFrame(x, y, width, height);
        g2.fill(ellipse);
        g2.draw(ellipse);

        g.setColor(Color.black);
        g.drawString(label.getText(), x + width / 2 - fm.stringWidth(label.getText()) / 2, y + height / 2);
    }

    /*
    * Funkce pro ziskani stredoveho bodu
    */
    @Override
    public Point getCenteredPoint() {
        return new Point(x + (width / 2), y + (height / 2));
    }

    /*
     * Funkce pro nastaveni souradnic 
    */
    public void setCoordinates(Point p) {
        x = p.x;
        y = p.y;
        ellipse.setFrame(x /*- (width / 2)*/, y /*- (height / 2)*/, width, height);
    }
}
