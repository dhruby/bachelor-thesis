/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package ToolBar.useCase;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.util.UUID;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/*
 * Trida reprezentujiciho aktera v diagramu uziti
*/
public class Actor extends MainObject {

    private int marginBottom = 15;

    /*
     * Zakladni konstruktor
    */
    public Actor() {
        super();
        img = new ImageIcon(this.getClass().getClassLoader().getResource("resources/person.png"), "Actor");

        width = img.getIconWidth();
        height = img.getIconHeight() + marginBottom + 5;
        label = new JLabel("");
    }

    /*
     * Kostruktor ID, slouzi pro import
    */
    public Actor(UUID id) {
        this.id = id;
        img = new ImageIcon(this.getClass().getClassLoader().getResource("resources/person.png"), "Actor");

        width = img.getIconWidth();
        height = img.getIconHeight() + marginBottom + 5;
        label = new JLabel("");
    }

    /*
     * Konstruktor se souradnicemi
    */
    public Actor(int x, int y) {
        super(x, y);
        img = new ImageIcon(this.getClass().getClassLoader().getResource("resources/person.png"), "Actor");

        this.x = x;
        this.y = y;

        label = new JLabel("User");

        width = img.getIconWidth();
        height = img.getIconHeight() + marginBottom + 5;
    }

    /*
     * Funkce pro vykresleni objektu
    */
    @Override
    public void draw(Graphics g) {
        FontMetrics fm = g.getFontMetrics();
        int textWidth = fm.stringWidth(label.getText());
        g.drawImage(img.getImage(), x, y, null);
        g.drawString(label.getText(), getCenteredPoint().x - textWidth / 2, y + img.getIconHeight() + marginBottom);
    }

    /*
     * Funkce pro ziskani stredoveho bodu
    */
    public Point getCenteredPoint() {
        int centeredX = x + (img.getIconWidth() / 2);
        int centeredY = y + (img.getIconHeight() / 2);

        return new Point(centeredX, centeredY);
    }

    /*
     * Funkce pro ziskani souradnic
    */
    public void setCoordinates(Point p) {
        x = p.x;
        y = p.y;
    }
}
