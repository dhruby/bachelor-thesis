/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package ToolBar.useCase;

import TabbedPanel.UCTab;
import ToolBar.Button;
import ToolBar.ToolBar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/*
 * Trida predstavujici hranu typu extend
*/
public class Extend extends Button {

    public Extend(String text, String image, ToolBar parentToolBar) {
        super(text, image, parentToolBar);
        addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                UCTab.drawLine = true;
            }
        });
    }
}
