/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package ToolBar.useCase;

import java.awt.BasicStroke;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.UUID;
import javax.swing.JLabel;

/*
 * Trida predstavujici spojovaci hranu
 */
public class Line extends MainObject {

    private final int ARR_SIZE = 7;
    private String type;
    public MainObject startObject;
    public MainObject endObject;
    private UUID startObjectID;
    private UUID endObjectID;
    private int startX;
    private int startY;
    private int endX;
    private int endY;

    /*
     * Zakladni konstruktor
     */
    public Line() {
        super();
        this.type = "Association";
        label = new JLabel("");
    }

    /*
     * Konstruktor s ID, slouzi pri importu
     */
    public Line(UUID id) {
        this.type = "Association";
        label = new JLabel("");
    }

    /*
     * Konstruktor s nastavenim typu
     */
    public Line(String type) {
        super();
        this.type = type;
        label = new JLabel("");
    }

    /*
     * Funkce pro vykresleni cary
     */
    @Override
    public void draw(Graphics g) {
        switch (type) {
            case "Association":
                drawAssociation(g, getStartX(), getStartY(), getEndX(), getEndY());
                break;
            case "Generalization":
                drawGeneralization(g, getStartX(), getStartY(), getEndX(), getEndY());
                break;
            case "Include":
                drawInclude(g, (int) getStartX(), (int) getStartY(), (int) getEndX(), (int) getEndY());
                break;
            case "Extend":
                drawExtend(g, (int) getStartX(), (int) getStartY(), (int) getEndX(), (int) getEndY());
                break;
            case "Arc":
                drawArc(g, getStartX(), getStartY(), getEndX(), getEndY());
                break;
        }
    }

    /*
     * Funkce pro vykresleni asociace
     */
    private void drawAssociation(Graphics g, double x1, double y1, double x2, double y2) {
        g.drawLine((int) x1, (int) y1, (int) x2, (int) y2);
    }

    /*
     * Funkce pro vykresleni generalizace
     */
    private void drawGeneralization(Graphics g1, double x1, double y1, double x2, double y2) {
        Graphics2D g = (Graphics2D) g1.create();

        double dx = x2 - x1, dy = y2 - y1;
        double angle = Math.atan2(dy, dx);
        int len = (int) Math.sqrt(dx * dx + dy * dy);
        AffineTransform at = AffineTransform.getTranslateInstance(x1, y1);
        at.concatenate(AffineTransform.getRotateInstance(angle));
        g.transform(at);

        g.drawLine(0, 0, len, 0);
        g.fillPolygon(new int[]{len, len - ARR_SIZE, len - ARR_SIZE, len},
                new int[]{0, -ARR_SIZE, ARR_SIZE, 0}, 4);
    }

    /*
     * Funkce pro vykresleni include
     */
    private void drawInclude(Graphics g, int x1, int y1, int x2, int y2) {
        Graphics2D g2d = (Graphics2D) g.create();
        FontMetrics fm = g.getFontMetrics();

        double dx = x2 - x1, dy = y2 - y1;
        double angle = Math.atan2(dy, dx);
        int len = (int) Math.sqrt(dx * dx + dy * dy);
        AffineTransform at = AffineTransform.getTranslateInstance(x1, y1);
        at.concatenate(AffineTransform.getRotateInstance(angle));
        g2d.transform(at);

        Stroke dashed = new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0);
        g2d.setStroke(dashed);
        g2d.drawLine(0, 0, len, 0);
        g2d.fillPolygon(new int[]{len, len - ARR_SIZE, len - ARR_SIZE, len},
                new int[]{0, -ARR_SIZE, ARR_SIZE, 0}, 4);

        g2d.drawString("<< include >>", (len / 2) - (fm.stringWidth("<< include >>") / 2), -5);
        g2d.dispose();
    }

    /*
     * Funkce pro vykresleni extend
     */
    private void drawExtend(Graphics g, int x1, int y1, int x2, int y2) {
        Graphics2D g2d = (Graphics2D) g.create();
        FontMetrics fm = g.getFontMetrics();

        double dx = x2 - x1, dy = y2 - y1;
        double angle = Math.atan2(dy, dx);
        int len = (int) Math.sqrt(dx * dx + dy * dy);
        AffineTransform at = AffineTransform.getTranslateInstance(x1, y1);
        at.concatenate(AffineTransform.getRotateInstance(angle));
        g2d.transform(at);

        Stroke dashed = new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0);
        g2d.setStroke(dashed);
        g2d.drawLine(0, 0, len, 0);
        g2d.drawString("<< extend >>", (len / 2) - (fm.stringWidth("<< extend >>") / 2), -5);

        g2d.fillPolygon(new int[]{len, len - ARR_SIZE, len - ARR_SIZE, len},
                new int[]{0, -ARR_SIZE, ARR_SIZE, 0}, 4);

        g2d.dispose();
    }

    /*
     * Funkce pro vykresleni spojovaci hrany v Petriho siti
     */
    private void drawArc(Graphics g1, double x1, double y1, double x2, double y2) {
        Graphics2D g = (Graphics2D) g1.create();
        FontMetrics fm = g.getFontMetrics();

        double dx = x2 - x1, dy = y2 - y1;
        double angle = Math.atan2(dy, dx);
        int len = (int) Math.sqrt(dx * dx + dy * dy);
        AffineTransform at = AffineTransform.getTranslateInstance(x1, y1);
        at.concatenate(AffineTransform.getRotateInstance(angle));
        g.transform(at);

        g.drawLine(0, 0, len, 0);
        if (!getLabelText().equals("")) {
            g.drawString(getLabelText(), (len / 2) - (fm.stringWidth(getLabelText()) / 2), -5);
        }
        g.fillPolygon(new int[]{len, len - ARR_SIZE, len - ARR_SIZE, len},
                new int[]{0, -ARR_SIZE, ARR_SIZE, 0}, 4);
    }

    /*
     * Funkce pri zjisteni kliknuti na hranu
     */
    public boolean contains(Point p) {
        int rectangleWidth = 10;

        int x = p.x - rectangleWidth / 2;
        int y = p.y - rectangleWidth / 2;

        Line2D l = new Line2D.Double(getStartX(), getStartY(), getEndX(), getEndY());

        return l.intersects(x, y, rectangleWidth, rectangleWidth);
    }

    /*
    * Funkce pro ziskani typu hrany
     */
    public void setType(String type) {
        this.type = type;
    }

    /*
     * Funkce pro nastaveni pocatecniho bodu
     */
    public void setStartPoint(Point p) {
        setStartX(p.x);
        setStartY(p.y);
    }

    /*
     * Funkce pro nastaveni koncoveho bodu
     */
    public void setEndPoint(Point p) {
        setEndX(p.x);
        setEndY(p.y);
    }

    /*
     * Funkce pro nastaveni pocatecniho objektu
     */
    public void setStartObject(MainObject object) {
        startObject = object;
    }

    /*
     * Funkce pro nastaveni koncoveho objektu
     */
    public void setEndObject(MainObject object) {
        endObject = object;
    }

    /*
     * Funkce pro ziskani pocatecniho bodu
     */
    public Point getStartPoint() {
        return new Point((int) getStartX(), (int) getStartY());
    }

    /*
     * Funkce pro ziskani koncoveho bodu
     */
    public Point getEndPoint() {
        return new Point((int) getEndX(), (int) getEndY());
    }

    /*
     * Funkce pro ziskani pocatecniho objektu
     */
    public MainObject getStartObject() {
        return startObject;
    }

    /*
     * Funkce pro ziskani koncoveho objektu
     */
    public MainObject getEndObject() {
        return endObject;
    }

    /*
     * Funkce pro nastaveni souradnic
    */
    public void setCoordinates() {
        Rectangle startRec = new Rectangle(startObject.x - 1, startObject.y - 1, startObject.width + 2, startObject.height + 2);
        Rectangle endRec = new Rectangle(endObject.x, endObject.y, endObject.width, endObject.height);
        if (!startRec.intersects(endRec)) {
            setStartCoordinates();
            setEndCoordinates();
        }

    }

    /*
     * Funkce pro nastaveni pocatecnich souradnic 
    */
    public void setStartCoordinates() {
        MainObject obj = getStartObject();
        setStartPoint(computetPoint(new Rectangle(obj.getX(), obj.getY(), obj.getWidth(), obj.getHeight()), obj.getCenteredPoint().x, obj.getCenteredPoint().y, (int) getEndX(), (int) getEndY()));
    }

    /*
     * Funkce pro nastaveni koncovych souradnic 
    */
    public void setEndCoordinates() {
        MainObject obj = getEndObject();
        setEndPoint(computetPoint(new Rectangle(obj.getX(), obj.getY(), obj.getWidth(), obj.getHeight()), obj.getCenteredPoint().x, obj.getCenteredPoint().y, (int) getStartX(), (int) getStartY()));
    }

    /*
     * Funkce pro vypocet pruseciku
    */
    private Point computetPoint(Rectangle r, int x, int y, int x2, int y2) {
        int w = x2 - x;
        int h = y2 - y;
        int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
        if (w < 0) {
            dx1 = -1;
        } else if (w > 0) {
            dx1 = 1;
        }
        if (h < 0) {
            dy1 = -1;
        } else if (h > 0) {
            dy1 = 1;
        }
        if (w < 0) {
            dx2 = -1;
        } else if (w > 0) {
            dx2 = 1;
        }
        int longest = Math.abs(w);
        int shortest = Math.abs(h);
        if (!(longest > shortest)) {
            longest = Math.abs(h);
            shortest = Math.abs(w);
            if (h < 0) {
                dy2 = -1;
            } else if (h > 0) {
                dy2 = 1;
            }
            dx2 = 0;
        }
        int numerator = longest >> 1;
        for (int i = 0; i <= longest; i++) {
            if (!r.getBounds().contains(new Point(x, y))) {
                return new Point(x, y);
            }
            numerator += shortest;
            if (!(numerator < longest)) {
                numerator -= longest;
                x += dx1;
                y += dy1;
            } else {
                x += dx2;
                y += dy2;
            }
        }
        return null;
    }

    public int getStartX() {
        return startX;
    }

    public void setStartX(int startX) {
        this.startX = startX;
    }

    public int getStartY() {
        return startY;
    }

    public void setStartY(int startY) {
        this.startY = startY;
    }

    public int getEndX() {
        return endX;
    }

    public void setEndX(int endX) {
        this.endX = endX;
    }

    public int getEndY() {
        return endY;
    }

    public void setEndY(int endY) {
        this.endY = endY;
    }

    public String getType() {
        return type;
    }

    public UUID getStartObjectID() {
        return startObjectID;
    }

    public void setStartObjectID(UUID startObjectID) {
        this.startObjectID = startObjectID;
    }

    public UUID getEndObjectID() {
        return endObjectID;
    }

    public void setEndObjectID(UUID endObjectID) {
        this.endObjectID = endObjectID;
    }

}
