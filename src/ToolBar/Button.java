/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package ToolBar;

import TabbedPanel.UCTab;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/*
 * Trida pro tlacitka v toolbaru
*/
public class Button extends JButton {

    private String image;
    private String text;
    private ToolBar parentToolBar;

    /*
     * konstruktor - nastaveni textu, ikony tlacitka a action listeneru
    */
    public Button(String text, String image, ToolBar parentToolBar) {
        super();

        this.text = text;
        this.image = image;
        this.parentToolBar = parentToolBar;

        init(text, image);
        addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                parentToolBar.setButtonsBackgrounds();
                if (TabbedPanel.Tab.activeBtn == "" || TabbedPanel.Tab.activeBtn != text) {
                    TabbedPanel.UCTab.activeBtn = text;
                    setBackground(Color.GRAY);
                } else {
                    TabbedPanel.UCTab.activeBtn = "";
                }
                setDrawLine();
            }
        });
    }

    /*
     * nastaveni tlacitka
    */
    private void init(String text, String image) {
        if (!image.equals("")) {
            ImageIcon icon = new ImageIcon(this.getClass().getClassLoader().getResource("resources/" + image), text);
            Image img = icon.getImage();
            Image newimg = img.getScaledInstance(30, 20, java.awt.Image.SCALE_SMOOTH);
            icon = new ImageIcon(newimg);
            this.setIcon(icon);
        }

        this.setText(text);

        this.setVerticalTextPosition(JButton.CENTER);
        this.setHorizontalTextPosition(JButton.RIGHT);
    }

    public void setDrawLine() {
        if (TabbedPanel.UCTab.activeBtn != "" && (text == "Association" || text == "Generalization" || text == "Include" || text == "Extend")) {
            UCTab.drawLine = true;
        } else {
            UCTab.drawLine = false;
        }
    }

}
