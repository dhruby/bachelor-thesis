/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package ToolBar;

import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JToolBar;

import java.util.ArrayList;


/*
 * Toolbar obsahujici tlacitka tabu
*/
public class ToolBar extends JToolBar {

    private ButtonManager btnManager;
    private ArrayList<JButton> btnList;

    /*
     * Nastaveni tlacitek v konstruktoru
    */
    public ToolBar(int orientation, int index) {
        super(orientation);
        
        setLayout(new GridLayout(0, 1));
        this.setFloatable(false);
        this.setMargin(new Insets(20, 0, 0, 0));
        this.btnManager = new ButtonManager(this);

        switch (index) {
            case 1:
                btnList = btnManager.addUseCaseButtons();
                break;
            case 2:
                btnList = btnManager.addClassDiagramButtons();
                break;
            case 3:
                btnList = btnManager.addOOPNButtons();
                break;
        }

        this.setButtons();
        addSeparator();
    }

    /*
     * Funkce pro pridani tlacitek
    */
    public void setButtons() {
        for (JButton btn : this.btnList) {
            this.add(btn);
        }
        this.repaint();
    }

    /*
     * Funkce pro nastaveni defaultniho pozadi tlacitek
    */
    public void setButtonsBackgrounds() {
        for (JButton btn : this.btnList) {
            btn.setBackground(null);
        }
        this.repaint();
    }
}
