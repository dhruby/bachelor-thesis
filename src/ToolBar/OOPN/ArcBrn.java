/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package ToolBar.OOPN;

import TabbedPanel.UCTab;
import ToolBar.Button;
import ToolBar.ToolBar;

/*
 * Tlačítko pro spojení objektů v Petriho sítí
*/
public class ArcBrn extends Button{
    public ArcBrn(String text, String image, ToolBar parentToolBar) {      
        super(text, image, parentToolBar);
    }     
    
}
