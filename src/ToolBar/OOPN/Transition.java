/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package ToolBar.OOPN;

import ToolBar.useCase.MainObject;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.util.UUID;
import javax.swing.JLabel;

/*
 * Trida pro prechod v OOPN
*/
public class Transition extends MainObject {

    private String guard = "";
    private String action = "";
    private int attributeHeight = 15;
    private int margin = 5;

    /*
     * Konstruktor s nastavenim souradnic
    */
    public Transition(int x, int y) {
        super(x, y);

        width = 20;
        height = 40;
        label = new JLabel("");
    }

    /*
     * Funkce pro vytvoreni prechodu
    */
    public Transition() {
        label = new JLabel("");
        width = 20;
        height = 40;
    }

    /*
     * Funkce pro vytvoreni prechodi s danym id, slouzi pro import
    */
    public Transition(UUID id) {
        this.id = id;
        label = new JLabel("");
        width = 20;
        height = 40;
    }

    /*
     * Funkce pro vykresleni prechodu
    */
    public void draw(Graphics g) {
        FontMetrics fm = g.getFontMetrics();
        attributeHeight = fm.getHeight();
        computeWidth(fm);
        computeHeight();

        int guardHeigth = 0;

        if (!guard.equals("")) {
            g.drawString(guard, x + margin, y + fm.getHeight());
            g.drawLine(x + margin, y + fm.getHeight() + 2, x + width - 4, y + fm.getHeight() + 2);
            guardHeigth = fm.getHeight();
        }

        if (!action.equals("")) {
            g.drawString(action, x + margin, y + guardHeigth + fm.getHeight() + margin);
        }

        if (!label.getText().equals("")) {
            g.drawString(label.getText(), getCenteredPoint().x - fm.stringWidth(label.getText()) / 2, y + height + fm.getHeight());
        }

        g.drawRect(x, y, width, height);
    }

    public void draw(Graphics g, int drawingWidth) {
        FontMetrics fm = g.getFontMetrics();
        int offset = drawingWidth / 2;
        attributeHeight = fm.getHeight();
        computeWidth(fm);
        computeHeight();

        int guardHeigth = 0;

        if (!guard.equals("")) {
            g.drawString(guard, x + offset + margin, y + fm.getHeight());
            g.drawLine(x+ offset + margin, y + fm.getHeight() + 2, x+ offset + width - 4, y + fm.getHeight() + 2);
            guardHeigth = fm.getHeight();
        }

        if (!action.equals("")) {
            g.drawString(action, x+ offset + margin, y + guardHeigth + fm.getHeight() + margin);
        }

        if (!label.getText().equals("")) {
            g.drawString(label.getText(), getCenteredPoint().x+ offset - fm.stringWidth(label.getText()) / 2, y + height + fm.getHeight());
        }

        g.drawRect(x+ offset, y, width, height);
    }
    
    /*
     * Vypocita sirku prechodu podle obsahu
    */
    private void computeWidth(FontMetrics fm) {
        if (fm.stringWidth(guard) > fm.stringWidth(action)) {
            width = fm.stringWidth(guard) + 10;
        } else {
            width = fm.stringWidth(action) + 10;
        }

        if (width < 20) {
            width = 20;
        }
    }

    /*
     * Vypocita vysku prechodu podle obsahu
    */
    private void computeHeight() {
        height = 10;
        if (!guard.equals("")) {
            height += 20;
        }
        if (!action.equals("")) {
            height += 20;
        }

        if (height < 40) {
            height = 40;
        }

    }

    /*
     * Funkce pro ziskani stredoveho bodu
    */
    public Point getCenteredPoint() {
        int centeredX = x + width / 2;
        int centeredY = y + height / 2;

        return new Point(centeredX, centeredY);
    }

    /*
     * Funkce pro ziskani straze
    */
    public String getGuard() {
        return guard;
    }

    /*
     * Funkce pro nastaveni straze
    */
    public void setGuard(String guard) {
        this.guard = guard;
    }

    /*
     * Funkce pro ziskani akce
    */
    public String getAction() {
        return action;
    }

    /*
     * Funkce pro nastaveni akce
    */
    public void setAction(String action) {
        this.action = action;
    }

}
