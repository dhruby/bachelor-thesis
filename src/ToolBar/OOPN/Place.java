/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package ToolBar.OOPN;

import ToolBar.useCase.Line;
import ToolBar.useCase.MainObject;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.UUID;
import javax.swing.JLabel;

/*
 * Objekt představující místo v Petriho sítí
 */
public class Place extends MainObject {

    private int marginBottom = 10;

    protected ArrayList<MainObject> pnList = new ArrayList<MainObject>();
    protected ArrayList<Line> pnLineList = new ArrayList<Line>();

    /*
     * Konstruktor
     */
    public Place() {
        label = new JLabel("");
        width = 30;
        height = 30;
    }

    /*
     * Konstruktor pro import projektu
     */
    public Place(UUID id) {
        this.id = id;
        label = new JLabel("");
        width = 30;
        height = 30;
    }

    /*
     * Konstruktor s nastavenim souradnic
     */
    public Place(int x, int y) {
        super(x, y);
        width = 30;
        height = 30;
        label = new JLabel("");
    }

    public Place(int x, int y, String text) {
        super(x, y);
        width = 30;
        height = 30;
        label = new JLabel(text);
    }

    /*
     * Prepsana metoda z predka, vykresli objekt
     */
    @Override
    public void draw(Graphics g) {
        FontMetrics fm = g.getFontMetrics();
        int textWidth = fm.stringWidth(label.getText());

        g.drawOval(x, y, width, width);
        g.drawString(label.getText(), getCenteredPoint().x - textWidth / 2, y + height + marginBottom);
    }

    public void draw(Graphics g, int drawingWidth) {
        FontMetrics fm = g.getFontMetrics();
        int textWidth = fm.stringWidth(label.getText());
        int offset = drawingWidth / 2;
        g.drawOval(x + offset, y, width, width);
        g.drawString(label.getText(), getCenteredPoint().x + offset - textWidth / 2, y + height + marginBottom);
    }

    /*
    * Vykresli siť metody
     */
    public void drawNet(Graphics g, int drawingWidth) {
        for (MainObject object : pnList) {
            if (object instanceof Place) {
                ((Place) object).draw(g, drawingWidth);
            } else if (object instanceof Transition) {
                ((Transition) object).draw(g, drawingWidth);
            }
        }

        for (Line l : pnLineList) {
            l.draw(g);
        }
    }

    /*
     * Funkce pro ziskani stredu objektu
     */
    public Point getCenteredPoint() {
        int centeredX = x + width / 2;
        int centeredY = y + height / 2;

        return new Point(centeredX, centeredY);
    }

    /*
     * Funkce pro ziskani objektu site metody 
     */
    public ArrayList<MainObject> getPnList() {
        return pnList;
    }

    /*
     * Funkce pro ziskani spojovacich hran site metody
     */
    public ArrayList<Line> getPnLineList() {
        return pnLineList;
    }

    /*
     * Zjisti, zdali bylo kliknuto na objekt
     */
    public boolean isClickOnObject(Point p, int drawingBoardWidth) {
        int offset = drawingBoardWidth / 2;
        if (new Rectangle(x, y, width, height).getBounds().contains(p)) {
            return true;
        }

        for (MainObject object : pnList) {
            if (new Rectangle(object.getX() + offset, object.getY(), object.getWidth(), object.getHeight()).getBounds().contains(p)) {
                return true;
            }
        }

        return false;
    }

    /*
     * Pridani spojovaci hrany k metode
     */
    public void addLineToPlace(Line l) {
        pnLineList.add(l);
    }

    /*
     * Funkce pro nastaveni mist metody, slouzi pri importu
     */
    public void setPnList(ArrayList<MainObject> pnList) {
        this.pnList = pnList;
    }

    /*
    * Funkce pro nastaveni hran metody, slouzi pri importu
     */
    public void setPnLineList(ArrayList<Line> pnLineList) {
        this.pnLineList = pnLineList;
    }
}
