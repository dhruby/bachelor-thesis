/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package ToolBar.classDiagram;

import ToolBar.useCase.MainObject;
import java.util.UUID;
import javax.swing.JLabel;

/*
 * Trida reprezentujici metodu v tride
*/
public class Method extends MainObject {

    private String access;
    private String name;
    private String dataType;

    /*
     * Konstruktor s nastavenim zakladnim vlastnosti
    */
    public Method(String access, String name, String dataType) {
        super();
        this.access = access;
        this.name = name;
        this.dataType = dataType;
    }

    /*
     * Konstruktor s nastavenim zakladnich vlastnosti a ID, slouzi pro import
    */
    public Method(UUID id,String access, String name, String dataType) {
        this.id = id;
        this.access = access;
        this.name = name;
        this.dataType = dataType;
        label = new JLabel();
    }

    /*
     * Funkce pro ziskani identifiaktoru pristupu
    */
    public String getAccess() {
        return access;
    }

    /*
     * Funkce pro nastaveni identifikatoru pristupu
    */
    public void setAccess(String access) {
        this.access = access;
    }

    /*
     * Funkce pro ziskani jmena metody
    */
    public String getName() {
        return name;
    }

    /*
     * Funkce pro nastaveni jmena metody
    */
    public void setName(String name) {
        this.name = name;
    }

    /*
     * Funkce pro ziskani navratoveho typu metody
    */
    public String getDataType() {
        return dataType;
    }

    /*
     * Funkce pro nastaveni navratoveho typu metody
    */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /*
     * Funkce pro ziskani stringu reprezentujiciho metodu
    */
    public String getStringMethod() {
        return access + " " + name + " : " + dataType;
    }
}
