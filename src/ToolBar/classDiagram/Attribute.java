/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package ToolBar.classDiagram;

import ToolBar.useCase.MainObject;
import java.util.UUID;
import javax.swing.JLabel;

/*
 * Trida predstavujici atribut ve tride 
 */
public class Attribute extends MainObject {

    private String access;
    private String name;
    private String dataType;

    /*
     * Konstruktor pro vytvoreni atributu s nastavenim zakladnich vlastnosti
     */
    public Attribute(String access, String name, String dataType) {
        super();
        this.access = access;
        this.name = name;
        this.dataType = dataType;
    }

    /*
     * Konstruktor pro vytvoreni atributu s nastavenim zakladnich vlastnosti a ID
     * slouzi pro import
     */
    public Attribute(UUID id, String access, String name, String dataType) {
        this.id = id;
        this.access = access;
        this.name = name;
        this.dataType = dataType;
        label = new JLabel();
    }

    /*
     * Funkce pro ziskani identifikatoru pristupu
     */
    public String getAccess() {
        return access;
    }

    /*
     * Funkce pro nastaveni identifikatoru pristupu
     */
    public void setAccess(String access) {
        this.access = access;
    }

    /*
     * Funkce pro ziskani jmena
     */
    public String getName() {
        return name;
    }

    /*
     * Funkce pro nastaveni jmena
     */
    public void setName(String name) {
        this.name = name;
    }

    /*
     * Funkce pro ziskani typu
     */
    public String getDataType() {
        return dataType;
    }

    /*
     * Funkce pro nastaveni typu
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /*
     * Funkce pro ziskani retezce predstavujici atribut
    */
    public String getStringAttribute() {
        return access + " " + name + " : " + dataType;
    }

}
