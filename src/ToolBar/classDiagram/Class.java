/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package ToolBar.classDiagram;

import ToolBar.OOPN.Place;
import ToolBar.useCase.Line;
import ToolBar.useCase.MainObject;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.UUID;
import javax.swing.JLabel;

/*
 * Predstavuje tridu  a jeji Petriho sit v diagramu trid
 */
public class Class extends MainObject {

    protected ArrayList<Method> methodList = new ArrayList<Method>();
    protected ArrayList<Attribute> attributeList = new ArrayList<Attribute>();
    protected ArrayList<MainObject> pnList = new ArrayList<MainObject>();
    protected ArrayList<Line> pnLineList = new ArrayList<Line>();

    protected int attributeHeight = 15;
    protected int headHeight = 40;
    protected int margin = 5;

    /*
     * konstruktor se souradnicemi
     */
    public Class(int x, int y) {
        super(x, y);
        label = new JLabel("Class");

        width = 100;
        height = headHeight + methodList.size() * attributeHeight + attributeList.size() * attributeHeight + 2 * margin;
    }

    /*
     * KOnstruktor se souradnicemi, jmenem a asociovanim objektem, slouzi pri propojeni diagramu
     */
    public Class(int x, int y, String name, MainObject assocObject) {
        super(x, y);
        label = new JLabel(name);
        this.assocObject = assocObject;

        width = 100;
        height = headHeight + methodList.size() * attributeHeight + attributeList.size() * attributeHeight + 2 * margin;
    }

    /*
     * Zakladni konstruktor
     */
    public Class() {
        label = new JLabel("Class");
    }

    /*
     * Konstruktor s ID, slouzi pri importu
     */
    public Class(UUID id) {
        this.id = id;
        label = new JLabel("Class");
    }

    /*
     * Funkce pro vykresleni tridy
     */
    @Override
    public void draw(Graphics g) {
        FontMetrics fm = g.getFontMetrics();
        int i = 0;
        int maxMethodLen = 100;

        for (Attribute a : attributeList) {
            g.drawString(a.getStringAttribute(), x + margin, y + headHeight + attributeHeight + (attributeHeight * i));
            if (fm.stringWidth(a.getStringAttribute()) + margin * 5 > maxMethodLen) {
                maxMethodLen = fm.stringWidth(a.getStringAttribute()) + margin * 5;
            }
            i++;
        }

        i = 0;
        for (Method m : methodList) {
            g.drawString(m.getStringMethod(), x + margin, y + headHeight + attributeList.size() * attributeHeight + (attributeHeight * i) + margin + attributeHeight);
            if (fm.stringWidth(m.getStringMethod()) + margin * 5 > maxMethodLen) {
                maxMethodLen = fm.stringWidth(m.getStringMethod()) + margin * 5;
            }
            i++;
        }

        if (fm.stringWidth(label.getText()) + margin * 5 > maxMethodLen) {
            maxMethodLen = fm.stringWidth(label.getText()) + margin * 5;
        }

        width = maxMethodLen;

        g.drawRect(x, y, width, headHeight);
        g.drawRect(x, y + headHeight, width, attributeList.size() * attributeHeight + margin);
        g.drawRect(x, y + headHeight + attributeList.size() * attributeHeight + margin, width, methodList.size() * attributeHeight + margin);

        g.drawString(label.getText(), x + width / 2 - (fm.stringWidth(label.getText()) / 2), y + margin * 4);
    }

    /*
     * Funkce pro vykresleni site metody 
     */
    public void drawNet(Graphics g, String methodName, int drawingWidth) {
        if (methodName != "") {
            Place p = getPlaceByMethodName(methodName);
            if (p != null) {
                p.drawNet(g, drawingWidth);
            }
        }
    }

    /*
     * Funkce pro ziskani mista podle metody
     */
    public Place getPlaceByMethodName(String methodName) {
        for (MainObject object : pnList) {
            if (object.getLabelText().equals(methodName)) {
                return (Place) object;
            }
        }

        return null;
    }

    /*
     * Pridani objektu do Petriho site tridy
     */
    public void addObjectToMethodPlace(String methodName, MainObject object) {
        Place p = getPlaceByMethodName(methodName);
        if (object instanceof Line) {
            p.getPnLineList().add((Line) object);
        } else {
            p.getPnList().add(object);
        }
    }

    /*
     * Funkce pro pridani atributu
     */
    public void addAttribute(Attribute a) {
        attributeList.add(a);
        computeHeight();
    }

    /*
     * Funkce pro odebrani atributu a jeho mista v Petriho siti
     */
    public void removeAttribute(Attribute a) {
        Place p = (Place) a.getAssocObject();
        removeLines(p);
        attributeList.remove(a);
        pnList.remove(p);
        computeHeight();
    }

    /*
     * Funkce pro pridani metody
     */
    public void addMethod(Method m) {
        methodList.add(m);
        computeHeight();
    }

    /*
     * Funkce pro odebrani metody a jeji mista v Petriho siti
     */
    public void removeMethod(Method m) {
        Place p = (Place) m.getAssocObject();
        removeLines(p);
        pnList.remove(p);
        methodList.remove(m);
        computeHeight();
    }

    /*
     * Funkce pro vypocet vysky
     */
    public void computeHeight() {
        height = headHeight + methodList.size() * attributeHeight + attributeList.size() * attributeHeight + 2 * margin;
    }

    /*
     * Funkce pro ziskani stredoveho bodu
     */
    public Point getCenteredPoint() {
        int centeredX = x + width / 2;
        int centeredY = y + height / 2;

        return new Point(centeredX, centeredY);
    }

    /*
     * Zjisti, zdali bylo kliknuto na objekt
     */
    public boolean isClickedOnHead(Point p) {
        return new Rectangle(x, y, width, headHeight).getBounds().contains(p);
    }

    /*
     * Zjisti, jestli bylo kliknuto na atribut
     */
    public boolean isClickedOnAttribut(Point p) {
        if (attributeList.size() > 0) {
            return new Rectangle(x, y + headHeight, width, attributeList.size() * attributeHeight + margin).getBounds().contains(p);
        }
        return false;
    }

    /*
     * Vrati atribut na ktery bylo kliknuto
     */
    public Attribute getClickedAttribute(Point p) {
        int i = 0;
        for (Attribute a : attributeList) {
            if (new Rectangle(x, y + headHeight + (attributeHeight * i), width, attributeHeight).getBounds().contains(p)) {
                return a;
            }
            i++;
        }
        return null;
    }

    /*
     * Zjisti, zdali bylo kliknuto na metodu
     */
    public boolean isClickedOnMethod(Point p) {
        if (methodList.size() > 0) {
            return new Rectangle(x, y + headHeight + attributeList.size() * attributeHeight + margin, width, methodList.size() * attributeHeight + margin).getBounds().contains(p);
        }
        return false;
    }

    /*
     * Vrati kliknutou metodu
     */
    public Method getClickedMethod(Point p) {
        int i = 0;
        for (Method m : methodList) {
            if (new Rectangle(x, y + headHeight + (attributeHeight * i) + (attributeHeight * attributeList.size()) + margin, width, attributeHeight).getBounds().contains(p)) {
                return m;
            }
            i++;
        }
        return null;
    }

    /*
     * Funkce pro ziskani metod
     */
    public ArrayList<Method> getMethodList() {
        return methodList;
    }

    /*
     * Funkce pro nastaveni metod
     */
    public ArrayList<Attribute> getAttributeList() {
        return attributeList;
    }

    /*
     * Funkce pro ziskani objektu Petriho site tridy
     */
    public ArrayList<MainObject> getPnlist() {
        return pnList;
    }

    /*
     * Funkce pro vygenerovani prislusnych mist v siti po preknuti zalozky
     */
    public void generatePlaces() {
        int i = 0;
        for (Method m : methodList) {
            Place p = new Place(i * 40, 50, m.getName());
            p.setAssocObject(m);
            p.setAssocObjectID(m.getId());
            m.setIsGeneratedAssocObject(true);
            m.setAssocObject(p);
            m.setAssocObjectID(p.getId());
            pnList.add(p);
            i++;
        }

        i = 0;
        for (Attribute a : attributeList) {
            Place p = new Place(i * 40, 90, a.getName());
            p.setAssocObject(a);
            p.setAssocObjectID(a.getId());

            a.setIsGeneratedAssocObject(true);
            a.setAssocObject(p);
            a.setAssocObjectID(p.getId());
            pnList.add(p);
            i++;
        }
    }

    /*
     * Funkce pro kontrolu mist v petriho siti
     */
    public void controlPlaces() {
        int i = 0;
        for (Method m : methodList) {
            if (!m.isIsGeneratedAssocObject()) {
                Place p = new Place(i * 40, 50, m.getName());
                p.setAssocObject(m);
                p.setAssocObjectID(m.getId());
                m.setAssocObject(p);
                m.setAssocObjectID(p.getId());
                m.setIsGeneratedAssocObject(true);
                pnList.add(p);
                i++;
            }
        }

        i = 0;
        for (Attribute a : attributeList) {
            if (!a.isIsGeneratedAssocObject()) {
                Place p = new Place(i * 40, 90, a.getName());
                p.setAssocObject(a);
                p.setAssocObjectID(a.getId());
                a.setAssocObject(p);
                a.setAssocObjectID(p.getId());
                a.setIsGeneratedAssocObject(true);
                pnList.add(p);
                i++;
            }
        }
    }

    /*
     * Funkce pro ziskani objektu Petriho site
     */
    public ArrayList<MainObject> getPnList() {
        return pnList;
    }

    /*
     * Funkce pro ziskani hran Petriho site
     */
    public ArrayList<Line> getPnLineList() {
        return pnLineList;
    }

    /*
     * Funkce pro odstaneni spojovacich hran v Petriho siti
     */
    private void removeLines(MainObject object) {
        ArrayList<Line> removableLines = new ArrayList<>();
        for (Line l : pnLineList) {
            if (l.getStartObject() == object) {
                removableLines.add(l);
            } else if (l.getEndObject() == object) {
                removableLines.add(l);
            }
        }

        pnLineList.removeAll(removableLines);
    }

    /*
     * Funkce pro nastaveni Petriho site tridy, slouzi pri importu
     */
    public void setPnList(ArrayList<MainObject> pnList) {
        this.pnList = pnList;
    }

    /*
     * Funkce pro nastaveni hran Petriho site tridy, slouzi pri importu
     */
    public void setPnLineList(ArrayList<Line> pnLineList) {
        this.pnLineList = pnLineList;
    }

    /*
     * Funkce vrati pole s metodama
    */
    public String[] getMethodsInArray() {
        String[] methods = new String[methodList.size() + 1];
        methods[0] = "";
        int i = 1;
        for (Method m : getMethodList()) {
            methods[i] = m.getName();
            i++;
        }

        return methods;
    }

}
