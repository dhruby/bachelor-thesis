/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package ToolBar.classDiagram;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.UUID;
import javax.swing.JLabel;

/*
 * Trida reprezentujici rozhrani
*/
public class Interface extends Class {

    private JLabel text;

    /*
     * Konstruktor se souradnicemi
    */
    public Interface(int x, int y) {
        super(x, y);

        text = new JLabel("<<Interface>>");
        label = new JLabel("Interface");
    }

    /*
     * Zakladni konstruktor 
    */
    public Interface() {
        label = new JLabel("Interface");
        text = new JLabel("<<Interface>>");
    }

    /*
     * Konstruktor s ID, slouzi pri importu
    */
    public Interface(UUID id) {
        this.id = id;
        label = new JLabel("Interface");
        text = new JLabel("<<Interface>>");
    }

    /*
     * Funkce pro vykresleni rozhrani
    */
    @Override
    public void draw(Graphics g) {
        FontMetrics fm = g.getFontMetrics();
        int i = 0;

        for (Attribute a : attributeList) {
            g.drawString(a.getStringAttribute(), x + margin, y + headHeight + attributeHeight + (attributeHeight * i));
            if (fm.stringWidth(a.getStringAttribute()) + margin * 5 > width) {
                width = fm.stringWidth(a.getStringAttribute()) + margin * 5;
            }
            i++;
        }

        i = 0;
        for (Method m : methodList) {
            g.drawString(m.getStringMethod(), x + margin, y + headHeight + attributeList.size() * attributeHeight + (attributeHeight * i) + margin + attributeHeight);
            if (fm.stringWidth(m.getStringMethod()) + margin * 5 > width) {
                width = fm.stringWidth(m.getStringMethod()) + margin * 5;
            }
            i++;
        }

        g.drawRect(x, y, width, headHeight);
        g.drawRect(x, y + headHeight, width, attributeList.size() * attributeHeight + margin);
        g.drawRect(x, y + headHeight + attributeList.size() * attributeHeight + margin, width, methodList.size() * attributeHeight + margin);

        g.drawString(text.getText(), x + width / 2 - (fm.stringWidth(text.getText()) / 2), y + margin * 4);
        g.drawString(label.getText(), x + width / 2 - (fm.stringWidth(label.getText()) / 2), y + margin * 6);
    }
}
