/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package MenuBar;

import TabbedPanel.PNTab;
import TabbedPanel.Tab;
import TabbedPanel.TabbedPanel;
import ToolBar.OOPN.Place;
import ToolBar.useCase.MainObject;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;
import xmlParsing.XMLParser;

/*
 * Trida pro menu v horni liste
 */
public class MenuFile extends JMenu {

    // nabidka menu
    private final JMenuItem save;
    private final JMenuItem saveGraphic;
    private final JMenuItem load;
    private final JMenuItem help;
    private final JMenuItem exit;

    private final TabbedPanel tabbedPanel;

    // trida pro import a export projektu
    private final XMLParser parser;

    /*
     * Konstruktor
     */
    public MenuFile(String s, TabbedPanel tabbedPanel) {
        super(s);

        this.save = new JMenuItem("Save project");
        this.saveGraphic = new JMenuItem("Save graphic");
        this.load = new JMenuItem("Load");
        this.help = new JMenuItem("Help");
        this.exit = new JMenuItem("Exit");

        this.tabbedPanel = tabbedPanel;
        this.parser = new XMLParser();

        init();
    }

    /*
     * Nastaveni tridy
     */
    private void init() {

        // Listener pro zavreni aplikace
        this.exit.addActionListener((ActionEvent event) -> {
            System.exit(0);
        });

        // Listener pro ulozeni projektu
        this.save.addActionListener((ActionEvent event) -> {
            try {
                // vyberove okno
                JFileChooser fc = new JFileChooser();
                fc.setFileSelectionMode(JFileChooser.FILES_ONLY);

                // nastaveni filtru
                fc.setFileFilter(new FileNameExtensionFilter("XML", "XML"));
                int returnVal = fc.showSaveDialog(tabbedPanel);

                // zpracovani vstupu
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    // pokud je spravny format, ulozi se projekt
                    if (fc.getFileFilter().getDescription().equals("XML")) {
                        parser.save(file, tabbedPanel.getUseCasePanel().getUCController().getObjectList(),
                                tabbedPanel.getUseCasePanel().getUCController().getLineList(),
                                tabbedPanel.getClassDiagramPanel().getCDController().getObjectList(),
                                tabbedPanel.getClassDiagramPanel().getCDController().getLineList(),
                                tabbedPanel.getOOPNPanel().getPNController().getObjectList(),
                                tabbedPanel.getClassDiagramPanel().getCDController().getLineList());
                        file.renameTo(new File(file.getAbsolutePath() + ".xml"));
                    } else {
                        int i = file.getName().lastIndexOf('.');
                        String extension = file.getName().substring(i + 1);
                        if (extension.equals("xml")) {
                            parser.save(file, tabbedPanel.getUseCasePanel().getUCController().getObjectList(),
                                    tabbedPanel.getUseCasePanel().getUCController().getLineList(),
                                    tabbedPanel.getClassDiagramPanel().getCDController().getObjectList(),
                                    tabbedPanel.getClassDiagramPanel().getCDController().getLineList(),
                                    tabbedPanel.getOOPNPanel().getPNController().getObjectList(),
                                    tabbedPanel.getClassDiagramPanel().getCDController().getLineList());
                        } else {
                            JOptionPane.showMessageDialog(MenuFile.this, "File does not contain valid XML!", "Warning!", JOptionPane.WARNING_MESSAGE);
                        }
                    }
                }
            } catch (ParserConfigurationException ex) {
                Logger.getLogger(MenuFile.class.getName()).log(Level.SEVERE, null, ex);
            } catch (TransformerException ex) {
                Logger.getLogger(MenuFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        // Listener pro import projektu
        this.load.addActionListener((ActionEvent event) -> {
            JFileChooser fc = new JFileChooser();
            fc.setFileSelectionMode(JFileChooser.FILES_ONLY);

            int returnVal = fc.showOpenDialog(tabbedPanel);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                try {
                    parser.load(file, tabbedPanel);
                    tabbedPanel.connectObjects();
                    tabbedPanel.repaint();
                } catch (ParserConfigurationException ex) {
                    Logger.getLogger(MenuFile.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SAXException ex) {
                    Logger.getLogger(MenuFile.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(MenuFile.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        });

        // Listener pro ulozeni prave aktualniho tabu do obrazku
        this.saveGraphic.addActionListener((ActionEvent event) -> {
            JFileChooser fc = new JFileChooser();
            fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fc.setFileFilter(new FileNameExtensionFilter("JPEG", "JPEG"));
            fc.setFileFilter(new FileNameExtensionFilter("PNG", "JPEG"));

            int returnVal = fc.showSaveDialog(tabbedPanel);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                int i = file.getName().lastIndexOf('.');
                String ext = "";
                String path = file.getAbsolutePath();
                if (fc.getFileFilter().getDescription().equals("PNG")) {
                    ext = "png";
                } else if (fc.getFileFilter().getDescription().equals("JPEG")) {
                    ext = "jpeg";
                } else if (i > -1) {
                    String extension = file.getName().substring(i + 1);
                    if (extension.toLowerCase().equals("png") || extension.equals("jpeg")) {
                        ext = extension.toLowerCase();
                    }
                }

                if (ext.toLowerCase().equals("png") || ext.toLowerCase().equals("jpeg")) {
                    BufferedImage image = new BufferedImage(tabbedPanel.getActivePanel().getDrawingBoardWidth(), tabbedPanel.getActivePanel().getDrawingBoardHeight(), BufferedImage.TYPE_INT_RGB);
                    Graphics2D g = image.createGraphics();
                    Tab tab = tabbedPanel.getActivePanel();

                    // pokud se jedna Petriho sit, ulozi se vsechny site metod
                    if (tab instanceof PNTab) {
                        PNTab pntab = (PNTab) tab;
                        for (MainObject o : pntab.getPNController().getObjectList()) {
                            if (o instanceof Place && ((Place) o).getPnList().size() > 0) {
                                pntab.setSelectedMethod(o.getLabelText());
                                image = new BufferedImage(tabbedPanel.getActivePanel().getDrawingBoardWidth(), tabbedPanel.getActivePanel().getDrawingBoardHeight(), BufferedImage.TYPE_INT_RGB);
                                g = image.createGraphics();
                                pntab.printAll(g);
                                try {
                                    ImageIO.write(image, ext, new File(path + "-" + o.getLabelText() + "." + ext));
                                } catch (IOException ex) {
                                    Logger.getLogger(MenuFile.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                g.dispose();
                            }
                        }
                        pntab.setSelectedMethod("");
                        pntab.repaint();
                    } else {
                        tabbedPanel.getActivePanel().printAll(g);
                        g.dispose();
                        try {
                            ImageIO.write(image, ext, new File(path + "."+ext));
                        } catch (IOException exp) {
                            exp.printStackTrace();
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(tabbedPanel, "File must be png or jpeg format!", "Warning!", JOptionPane.WARNING_MESSAGE);
                }
            }
        });

        // Listener pro zavolani napovedy
        this.help.addActionListener((ActionEvent event) -> {
                HelpDialog hd = new HelpDialog();
                hd.setVisible(true);
        });

        // Pridani k menu
        this.add(this.save);
        this.add(this.saveGraphic);
        this.add(this.load);
        this.add(this.help);
        this.add(this.exit);
    }

}
