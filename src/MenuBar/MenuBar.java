/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package MenuBar;

import TabbedPanel.TabbedPanel;
import javax.swing.JMenuBar;

/*
 * Trida pro horni listu
 */
public class MenuBar extends JMenuBar{

    private final MenuFile menuFile;
    private final TabbedPanel tabbedPanel;
    
    /*
     * Konstriktor
    */
    public MenuBar(TabbedPanel tabbedPanel) {
        this.menuFile = new MenuFile("File", tabbedPanel);
        this.tabbedPanel = tabbedPanel;
        init();
    }
    
    /*
     * Pridani menu k liste
    */
    private void init(){
        this.add(this.menuFile);
    }
    
}
