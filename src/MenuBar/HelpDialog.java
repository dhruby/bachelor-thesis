/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package MenuBar;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextArea;

/*
 * Trida pro zobrazeni napovedy
 */
class HelpDialog extends JDialog {

    /*
     * Konstruktor
     */
    public HelpDialog() {
        setTitle("Help");
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        String content = getHelpString();
        JTextArea textArea = new JTextArea(content);
        textArea.setEditable(false);
        add(textArea);

        JButton close = new JButton("Close");
        close.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                dispose();
            }
        });

        close.setAlignmentX(0.5f);
        add(close);
        setModalityType(ModalityType.APPLICATION_MODAL);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        //setSize(300, 200);
    }

    /*
     * Funkce pro nacteni retezce ze souboru
     */
    private String getHelpString() {
        return new String("-------------- BASIC INFORMATION --------------\n"
                + "For drawing object must be selected button from right panel\n"
                + "and then click on drawing board\n"
                + "\n"
                + "For move object click on object and drag\n"
                + "\n"
                + "For change object properties double click on object and\n"
                + "form for changes will be shown\n"
                + "\n"
                + "For delete object click on object and press delete key on\n"
                + "keybord\n"
                + "\n"
                + "For draw Petri net you must have select class");
    }
}
