/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package TabbedPanel;

import ToolBar.ToolBar;
import ToolBar.useCase.MainObject;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;

/*
 * Rodicovka trida pro vsechny taby
*/
abstract public class Tab extends JPanel {

    public static String activeBtn = "";

    protected ToolBar toolbar;
    protected int index;
    protected MainObject movingObject;
    protected MainObject selectedItem;

    /*
     * Konstruktor
    */
    public Tab(int index) {

        this.index = index;
        toolbar = new ToolBar(JToolBar.VERTICAL, index);

        this.setLayout(new BorderLayout());
        this.add(toolbar, BorderLayout.EAST);
        this.setBackground(Color.WHITE);
        setFocusable(true);

    }

    /*
     * Ziskani aktivniho tlacitka
    */
    public String getActiveBtn() {
        return activeBtn;
    }

    /*
     * Ziskani pohybujiciho objektu
    */
    public MainObject getMovingObject() {
        return movingObject;
    }

    /*
     * Nastaveni pohybujiciho projektu
    */
    public void setMovingObject(MainObject movingObject) {
        this.movingObject = movingObject;
    }

    /*
     * Funkce pro ziskani sirky panely bez toolbaru
    */
    public int getDrawingBoardWidth() {
        return super.getWidth() - toolbar.getWidth();
    }

    /*
    * Funkce pro ziskani vysky panelu
    */
    public int getDrawingBoardHeight() {
        return super.getHeight();
    }

    /*
     * Funkce pro ziskani toolbaru
    */
    public ToolBar getToolbar(){
        return toolbar;
    }
    
    /*
     * Abstraktni funkce pro vyber objektu
    */
    abstract public void setSelectedItem(MainObject object);
}
