/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package TabbedPanel;

import Controllers.CDController;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

/*
 * Mouse motion listener pro diagram trid
*/
public class CDTabMouseMotionListener implements MouseMotionListener {

    private CDTabMouseListener listener;
    private CDTab tab;
    private CDController CDController;

    /*
     * Konstruktor
    */
    public CDTabMouseMotionListener(CDTabMouseListener listener, CDTab tab, CDController CDController) {
        this.listener = listener;
        this.tab = tab;
        this.CDController = CDController;
    }

    /*
     * Pokud je zmačnute prave tlacitko mysy a je tazeno, vyvola se tato funkce
     * bud nakresli caru nebo posune objektem
    */
    @Override
    public void mouseDragged(MouseEvent e) {
        if (listener.isActiveLine()) {
            listener.setDrawingLine(e.getPoint());
            tab.repaint();
        } else if (tab.getActiveBtn() == "" && tab.isClickedOnObject(e.getPoint())) {
            tab.moveObject(e.getPoint());
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

}
