/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package TabbedPanel;

import Controllers.UCController;
import java.awt.*;
import java.awt.event.*;
import ToolBar.useCase.Actor;
import ToolBar.useCase.Line;
import ToolBar.useCase.UseCase;

/*
 * Mouse listener pro diagram trid
*/
public class UCTabMouseListener implements MouseListener {
    protected UCController UCController;
    protected Boolean isMouseReleased = false;
    protected int x, y;
    protected Point pointStart = null;
    protected Point pointEnd = null;
    protected UCTab tab;
    protected boolean drawLine = false;
    protected Line drawingLine;

    /*
     * Konstruktor
    */
    public UCTabMouseListener(UCController UCController, Line drawingLine, UCTab tab) {
        super();
        this.tab = tab;
        this.UCController = UCController;
        this.drawingLine = drawingLine;
    }

    /*
    * Funkce volana pri kliknuti tlacitka
    */
    public void mouseClicked(MouseEvent e) {
        x = e.getX();
        y = e.getY();
        if (e.getClickCount() == 2) {
            if (tab.isClickedOnObject(e.getPoint())) {
                UCController.displayForm(e.getPoint());
                //UCController.setSelectedItem(null);
            }
        } else if (tab.getActiveBtn() != "") {
            switch (tab.getActiveBtn()) {
                case "Actor":
                    UCTab.drawLine = false;
                    UCController.addObject(new Actor(x, y));
                    break;
                case "Use case":
                    UCTab.drawLine = false;
                    UCController.addObject(new UseCase(x, y));
                    break;
            }
            UCController.setSelectedItem(null);

        } else {
            UCController.setSelectedItem(UCController.getObjectByCoordinates(e.getPoint()));
            UCTab.drawLine = false;
        }
        tab.repaint();

    }

    /*
     * Funkce volana pri zmacknuti tlacitka
    */
    public void mousePressed(MouseEvent e) {
        if (isActiveLine() && tab.isClickedOnObject(e.getPoint())) {
            drawingLine = new Line(tab.getActiveBtn());
            drawingLine.setStartX(e.getX());
            drawingLine.setStartY(e.getY());
            tab.setDrawingLine(drawingLine);
        }else if(tab.getActiveBtn() == "" && tab.isClickedOnObject(e.getPoint())){
            tab.setMovingObject(UCController.getObjectByCoordinates(e.getPoint()));
            UCController.setSelectedItem(null);
        }else {
            drawingLine = null;
            tab.setDrawingLine(drawingLine);
        }
        //
    }
    
    
    public void mouseReleased(MouseEvent e) {
        if (isActiveLine() && tab.isClickedOnObject(e.getPoint()) && drawingLine != null) {
            drawingLine.setEndX(e.getX());
            drawingLine.setEndY(e.getY());
            Point p = new Point((int) drawingLine.getStartX(), (int) drawingLine.getStartY());

            //tab.addLineToObject(p, drawingLine);
            UCController.addLine(drawingLine);

            tab.setDrawingLine(new Line(tab.getActiveBtn()));

            tab.repaint();
        } else {
            drawingLine = null;
            tab.setDrawingLine(drawingLine);

            tab.repaint();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /*
     * Funkce pro zjisteni, zdali se ma kreslit cara
    */
    protected boolean isActiveLine() {
        if (tab.getActiveBtn() == "Association"
                || tab.getActiveBtn() == "Generalization"
                || tab.getActiveBtn() == "Include"
                || tab.getActiveBtn() == "Extend") {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Funkce pro nstaveni kreslici cary
    */
    protected void setDrawingLine(Point p) {
        if (drawingLine != null) {
            drawingLine.setEndX(p.x);
            drawingLine.setEndY(p.y);
            tab.setDrawingLine(drawingLine);
            tab.repaint();
        }
    }
}
