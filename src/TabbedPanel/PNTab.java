/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package TabbedPanel;

import Controllers.PNController;
import ToolBar.OOPN.Place;
import ToolBar.useCase.Line;
import ToolBar.useCase.MainObject;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import ToolBar.classDiagram.Class;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

/*
 * Zalozka pro Petriho site
 */
public class PNTab extends Tab {

    public static boolean drawLine = false;

    private PNController PNController;
    private PNTabMouseListener mouseListener;
    private Line drawingLine = null;
    private Line methodDrawingLine = null;
    private boolean drawline;
    private Class classItem = null;
    private JComboBox methodsComboBox = null;
    private JButton showBtn;
    private String selectedMethod = null;

    /*
     * Konstruktor
     */
    public PNTab(int index) {
        super(index);

        PNController = new PNController(this);
        mouseListener = new PNTabMouseListener(PNController, this);
        addMouseListener(mouseListener);
        addMouseMotionListener(new PNTabMouseMotionListener(mouseListener, this, PNController));
        setKeyBinding();

        // funkce pro pridani select boxu pro vyber metody
        addMethodForm();
    }

    /*
     * Funkce pro vykresleni zalozky
     * vykresluje dva panely - pro tridu a metody
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        super.paintComponent(g);

        for (Line l : PNController.getLineList()) {
            l.draw(g);
        }

        for (MainObject object : PNController.getObjectList()) {
            object.draw(g);
        }

        if (drawingLine != null) {
            drawingLine.draw(g);
        }

        if (PNController.getSelectedItem() != null) {
            if (PNController.getSelectedItem() instanceof Line) {
                PNController.highlightLine(g, PNController.getSelectedItem());
            } else {
                PNController.highlightItem(g, PNController.getSelectedItem());
                if (PNController.getSelectedItem() instanceof Place) {
                    Place p = (Place) PNController.getSelectedItem();
                    p.drawNet(g, getDrawingBoardWidth());
                }
            }
        }

        if (selectedMethod != null) {
            classItem.drawNet(g, selectedMethod, getDrawingBoardWidth());

        }

        g.setColor(Color.black);
        g.drawLine(getDrawingBoardWidth() / 2, 0, getDrawingBoardWidth() / 2, getDrawingBoardHeight());

    }

    /*
     * Funkce pro posun objektu
     */
    public void moveObject(Point p) {
        PNController.moveObjects(movingObject, p);

        repaint();
    }

    /*
     * Funkce pro nastaveni kreslici hrany
     */
    public void setDrawingLine(Line l) {
        drawingLine = l;
    }

    /*
     * Funkce pro zjisteni, zdali bylo kliknuto na objekt
     */
    protected Boolean isClickedOnObject(Point p) {
        for (MainObject object : PNController.getObjectList()) {
            if(object instanceof Place){
                 if (((Place)object).isClickOnObject(p, getDrawingBoardWidth())) {
                            return true;
                        }
            }else{
            if (object.isClickOnObject(p)) {
                return true;
            }
            }
            /*if (object instanceof Place) {
                for (MainObject methodObject : ((Place) object).getPnList()) {
                    if (object instanceof Place) {
                        if (((Place)object).isClickOnObject(p, getDrawingBoardWidth())) {
                            return true;
                        }
                    } else if (object.isClickOnObject(p)) {
                        return true;
                    }
                }
            }*/
        }

        return false;
    }

    /*
     * Nabindovani tlacitka Delete pro mazani vybranych objektu
     */
    private void setKeyBinding() {
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "deleteObject");
        this.getActionMap().put("deleteObject", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (PNController.getSelectedItem() != null) {
                    JDialog.setDefaultLookAndFeelDecorated(true);
                    int response = JOptionPane.showConfirmDialog(null, "Do you want to delete selected item? Action can affect others diagrams.", "Confirm delete item",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (response == JOptionPane.YES_OPTION) {
                        PNController.deleteSelectedItem(classItem, selectedMethod);
                        setMethodForm(selectedMethod);
                        repaint();
                    }
                } else if (PNController.getSelectedMethodItem() != null) {
                    JDialog.setDefaultLookAndFeelDecorated(true);
                    int response = JOptionPane.showConfirmDialog(null, "Do you want to delete selected item? Action can affect others diagrams.", "Confirm delete item",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (response == JOptionPane.YES_OPTION) {
                        PNController.deleteSelectedItem(classItem, selectedMethod);
                        setMethodForm(selectedMethod);
                        repaint();

                    }
                }
            }
        });
    }

    /*
     * Funkce pro nastaveni vybraneho objketu
     */
    public void setSelectedItem(MainObject object) {
        PNController.setSelectedItem(object);
    }

    /*
     * Funkce pro nastaveni tridy
     */
    public void setClass(Class c) {
        classItem = c;
    }

    /*
    * Funkce pro ziskani tridy
     */
    public Class getClassItem() {
        return classItem;
    }

    /*
     * Funkce pro generovani objektu po zmene zalozky
     */
    public void generatePN() {
        if (classItem.getPnlist().size() == 0) {
            classItem.generatePlaces();
        } else {
            classItem.controlPlaces();
        }
    }

    /*
    * Funkce pro nastaveni controleru pri importu
     */
    public void setController() {
        PNController.setLineList(classItem.getPnLineList());
        PNController.setObjectList(classItem.getPnList());
    }

    /*
     * Funkce pro nastaveni petriho siti tridy 
     */
    public void setClassItem() {
        classItem.setPnLineList(PNController.getLineList());
        classItem.setPnList(PNController.getObjectList());
    }

    /*
     * Funkce pro pridani select boxu pro vyber metody v OOPN
     */
    private void addMethodForm() {
        String[] methods;
        if (classItem == null) {
            methods = new String[0];
        } else {
            methods = classItem.getMethodsInArray();
        }

        DefaultComboBoxModel model = new DefaultComboBoxModel(methods);
        methodsComboBox = new JComboBox();
        methodsComboBox.setModel(model);
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(methodsComboBox);

        methodsComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                selectedMethod = (String) methodsComboBox.getSelectedItem();
                if (selectedMethod == "") {
                    selectedMethod = null;
                }
                repaint();
            }
        });
        toolbar.add(new JLabel("Methods: "));
        toolbar.add(panel);
    }

    /*
     * Funkce pro nastaveni formulare metody 
     */
    public void setMethodForm() {
        DefaultComboBoxModel model = (DefaultComboBoxModel) methodsComboBox.getModel();
        model.removeAllElements();
        for (String s : classItem.getMethodsInArray()) {
            model.addElement(s);
        }
        methodsComboBox.setModel(model);
    }

    /*
     * Funkce pro nastaveni formulare metody a nastaveni hodnoty
     */
    public void setMethodForm(String selectedMethod) {
        DefaultComboBoxModel model = (DefaultComboBoxModel) methodsComboBox.getModel();
        model.removeAllElements();
        int i = 0;
        int index = 0;
        for (String s : classItem.getMethodsInArray()) {
            model.addElement(s);
            if (s.equals(selectedMethod)) {
                index = i;
            }
            i++;

        }
        methodsComboBox.setModel(model);
        methodsComboBox.setSelectedIndex(index);
    }

    /*
     * Funkce pro ziskani vybraneho objektu
     */
    public String getSelectedMethod() {
        return selectedMethod;
    }

    /*
     * Funkce pro nastveni kliknuteho objektu
     */
    public void setSelectedMethod(String selectedMethod) {
        this.selectedMethod = selectedMethod;
    }

    /*
    * Funkce pro ziskani controleru
     */
    public PNController getPNController() {
        return PNController;
    }

    /*
     * Funkce pro nastaveni controleru
     */
    public void setPNController(PNController PNController) {
        this.PNController = PNController;
    }

    /*
     * Funkce pro ziskani kreslici hrany v siti metody 
     */
    public Line getMethodDrawingLine() {
        return methodDrawingLine;
    }

    /*
     * Funkce pro nastaveni kreslici hrany v siti metody 
     */
    public void setMethodDrawingLine(Line methodDrawingLine) {
        this.methodDrawingLine = methodDrawingLine;
    }
}
