/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package TabbedPanel;

import ToolBar.OOPN.Place;
import java.awt.event.KeyEvent;
import javax.swing.JTabbedPane;
import ToolBar.classDiagram.Attribute;
import ToolBar.useCase.MainObject;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import ToolBar.classDiagram.Class;
import ToolBar.classDiagram.Interface;
import ToolBar.classDiagram.Method;
import ToolBar.useCase.Line;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/*
 * Trida pro spravu zalozek
*/
public class TabbedPanel extends JTabbedPane {

    private int lastTab = 0;

    private UCTab useCasePanel;
    private CDTab classDiagramPanel;
    private PNTab OOPNPanel;

    /*
     * Konstruktor
    */
    public TabbedPanel() {
        super();
        init();

        // listener pro zmenu
        addChangeListener(new ChangeListener() {

            public void stateChanged(ChangeEvent e) {
                Tab.activeBtn = "";
                setButtonsToDefault();
                if (e.getSource() instanceof JTabbedPane) {
                    // pri zmene tabu vygenerovat prislusne objekty
                    switch (getSelectedIndex()) {
                        case 0:
                            useCasePanel.setSelectedItem(null);
                            break;
                        case 1:
                            classDiagramPanel.setSelectedItem(null);
                            classDiagramPanel.controllClasses();
                            classDiagramPanel.generateClasses(useCasePanel.getObjects());
                            break;
                        case 2:
                            MainObject object = classDiagramPanel.getSelectedItem();
                            if (object != null && object instanceof Class) {
                                OOPNPanel.setClass((Class) object);
                                OOPNPanel.generatePN();
                                OOPNPanel.setController();
                                OOPNPanel.setMethodForm();
                            } else {
                                setTab(1);
                                showWarningMessage("No class has been selected");
                            }
                            break;
                    }
                    if (lastTab == 3) {
                        OOPNPanel.setClassItem();
                        OOPNPanel.setSelectedMethod(null);
                    }
                    lastTab = getSelectedIndex();
                }
            }
        });
    }

    public enum e_Tabs {
        useCase, classDiagram, OOPN
    }

    private void init() {
        this.setTabPlacement(JTabbedPane.TOP);

        initPanels();
    }

    /*
     * Zakladni nastaveni tabu
    */
    private void initPanels() {
        this.useCasePanel = new UCTab(1);
        this.classDiagramPanel = new CDTab(2);
        this.classDiagramPanel.setFocusable(true);
        this.classDiagramPanel.requestFocus();
        this.OOPNPanel = new PNTab(3);

        this.addTab("Use case", this.useCasePanel);
        this.addTab("Class diagram", this.classDiagramPanel);
        this.addTab("OOPN", this.OOPNPanel);

        this.setMnemonicAt(0, KeyEvent.VK_U);
        this.setMnemonicAt(1, KeyEvent.VK_C);
        this.setMnemonicAt(2, KeyEvent.VK_O);
    }

    private void setTab(int index) {
        setSelectedIndex(index);
    }

    public UCTab getUseCasePanel() {
        return useCasePanel;
    }

    public void setUseCasePanel(UCTab useCasePanel) {
        this.useCasePanel = useCasePanel;
    }

    public CDTab getClassDiagramPanel() {
        return classDiagramPanel;
    }

    public void setClassDiagramPanel(CDTab classDiagramPanel) {
        this.classDiagramPanel = classDiagramPanel;
    }

    public PNTab getOOPNPanel() {
        return OOPNPanel;
    }

    public void setOOPNPanel(PNTab OOPNPanel) {
        this.OOPNPanel = OOPNPanel;
    }

    /*
     * Ziskani prave aktivniho tabu
    */
    public Tab getActivePanel() {
        switch (getSelectedIndex()) {
            case 0:
                return useCasePanel;
            case 1:
                return classDiagramPanel;
            case 2:
                return OOPNPanel;
        }
        return useCasePanel;
    }

    public void showWarningMessage(String message) {
        JOptionPane.showMessageDialog(this, message, "Warning!", JOptionPane.WARNING_MESSAGE);
    }

    /*
     * Nastaveni tlacitek
    */
    public void setButtonsToDefault() {
        useCasePanel.getToolbar().setButtonsBackgrounds();
        classDiagramPanel.getToolbar().setButtonsBackgrounds();
        OOPNPanel.getToolbar().setButtonsBackgrounds();
    }

    /*
     * Funkce pro propojeni objektu po importu objektu
    */
    public void connectObjects() {
        for (MainObject o : useCasePanel.getUCController().getObjectList()) {
            if (o.getAssocObjectID() != null) {
                o.setAssocObject(getAssocObject(o.getAssocObjectID().toString(), classDiagramPanel.getCDController().getObjectList()));
                o.getAssocObject().setAssocObject(o);
            }
        }

        for (MainObject o : classDiagramPanel.getCDController().getObjectList()) {
            if (o instanceof Class && !(o instanceof Interface)) {
                Class c = (Class) o;
                for (Attribute a : c.getAttributeList()) {
                    if (a.getAssocObjectID() != null) {
                        String aID = a.getAssocObjectID().toString();
                        a.setAssocObject(getAssocObject(a.getAssocObjectID().toString(), c.getPnList()));
                        a.getAssocObject().setAssocObject(a);
                    }
                }

                for (Method m : c.getMethodList()) {
                    if (m.getAssocObjectID() != null) {
                        m.setAssocObject(getAssocObject(m.getAssocObjectID().toString(), c.getPnList()));
                        m.getAssocObject().setAssocObject(m);
                    }
                }
            }
        }

        connectUseCaseLines(useCasePanel.getUCController().getLineList());
        connectClassDiagramLines(classDiagramPanel.getCDController().getLineList());
    }

    /*
     * funkce pro ziskani asociativniho objektu
    */
    public MainObject getAssocObject(String assocId, ArrayList<MainObject> objectList) {
        for (MainObject o : objectList) {
            String id = o.getId().toString();
            if (o.getId().toString().equals(assocId)) {
                return o;
            }
        }

        return null;
    }

    /*
     * Funkce pro propojeni objektu v diagramu uziti
    */
    private void connectUseCaseLines(ArrayList<Line> lineList) {
        for (Line l : lineList) {
            l.setStartObject(getObjectById(l.getStartObjectID().toString(), useCasePanel.getUCController().getObjectList()));
            l.setEndObject(getObjectById(l.getEndObjectID().toString(), useCasePanel.getUCController().getObjectList()));
        }
    }

    /*
     * Funkce pro propojeni diagramu trid a Petriho siti
    */
    private void connectClassDiagramLines(ArrayList<Line> lineList) {
        for (Line l : lineList) {
            l.setStartObject(getObjectById(l.getStartObjectID().toString(), classDiagramPanel.getCDController().getObjectList()));
            l.setEndObject(getObjectById(l.getEndObjectID().toString(), classDiagramPanel.getCDController().getObjectList()));
        }

        for (MainObject o : classDiagramPanel.getCDController().getObjectList()) {
            if (o instanceof Class) {
                Class c = (Class) o;
                for (Line l : c.getPnLineList()) {
                    l.setStartObject(getObjectById(l.getStartObjectID().toString(), c.getPnList()));
                    l.setEndObject(getObjectById(l.getEndObjectID().toString(), c.getPnList()));
                }

                for (MainObject PNobject : c.getPnList()) {
                    if (PNobject instanceof Place) {
                        Place p = (Place) PNobject;
                        if (p.getPnLineList().size() > 0) {
                            for (Line l : p.getPnLineList()) {
                                l.setStartObject(getObjectById(l.getStartObjectID().toString(), p.getPnList()));
                                l.setEndObject(getObjectById(l.getEndObjectID().toString(), p.getPnList()));
                            }
                        }
                    }
                }
            }
        }
    }

    /*
     * Funkce pro ziskani objektu podle id
    */
    private MainObject getObjectById(String id, ArrayList<MainObject> objectList) {
        
        for (MainObject o : objectList) {
            String ids = o.getId().toString();
            if (o.getId().toString().equals(id)) {
                return o;
            }
        }

        return null;
    }

}
