/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package TabbedPanel;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

/*
 * Mouse motion listener pro kresleni car v diagramu uziti
*/
public class UCTabMouseMotionListener implements MouseMotionListener{

    private UCTabMouseListener listener;
    private UCTab tab;
    
    public UCTabMouseMotionListener(UCTabMouseListener listener, UCTab tab) {
        this.listener = listener;
        this.tab = tab;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (listener.isActiveLine()) {
            listener.setDrawingLine(e.getPoint());
        }else if(tab.getActiveBtn() == "" && tab.isClickedOnObject(e.getPoint())){
            tab.moveObject(e.getPoint());
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
