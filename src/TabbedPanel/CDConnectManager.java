/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package TabbedPanel;

import ToolBar.useCase.MainObject;
import ToolBar.classDiagram.Class;
import ToolBar.classDiagram.Interface;

/*
 * Trida pro kontrolu spojovacich hran
*/
public class CDConnectManager {

    /*
     * Funkce pro zjisteni, zdali je dany vztah povolen
    */
    public boolean canBeConnected(MainObject startObject, MainObject endObject, String lineType) {
       if (startObject instanceof Interface && endObject instanceof Interface && lineType == "Generalization") {
            return true;
       }
        if (startObject instanceof Class && endObject instanceof Interface && lineType != "Generalization") {
            return false;
        } else if (startObject instanceof Interface && endObject instanceof Class && lineType != "Generalization") {
            return false;
        }
        return true;
    }
}
