/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package TabbedPanel;

import Controllers.UCController;
import ToolBar.useCase.Line;
import ToolBar.useCase.MainObject;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/*
 * tab pro diagram uziti
*/
public class UCTab extends Tab {

    public static boolean drawLine = false;

    private int index;
    private UCController UCController;
    private Line drawingLine;
    private UCTabMouseListener mouseListener;

    /*
     * Konstruktor
    */
    public UCTab(int index) {
        super(index);
        UCController = new UCController();
        mouseListener = new UCTabMouseListener(UCController, drawingLine, this);
        addMouseListener(mouseListener);
        addMouseMotionListener(new UCTabMouseMotionListener(mouseListener, this));
        setKeyBinding();
    }

    /*
    * Funkce pro vykresleni tabu
    */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        for (Line l : UCController.getLineList()) {
            l.draw(g);
        }

        for (MainObject object : UCController.getObjectList()) {
            object.draw(g);
        }

        if (drawingLine != null) {
            drawingLine.draw(g);
        }

        if (UCController.getSelectedItem() != null) {
            if (UCController.getSelectedItem() instanceof Line) {
                UCController.highlightLine(g, UCController.getSelectedItem());
            } else {
                UCController.highlightItem(g, UCController.getSelectedItem());

            }
        }
    }

    /*
     * Funkce pro nastaveni vybraneho objektu
    */
    public void setSelectedItem(MainObject object) {
        UCController.setSelectedItem(object);
    }

    /*
     * Funkce pro zjisteni, zdali bylo kliknuto na objekt
    */
    protected Boolean isClickedOnObject(Point p) {
        for (MainObject object : UCController.getObjectList()) {
            if (object.isClickOnObject(p)) {
                return true;
            }
        }

        return false;
    }

    /*
     * Funkce pro nastaveni kreslici cary
    */
    public void setDrawingLine(Line l) {
        drawingLine = l;
    }

    /*
     * Funkce pro posun objektu
    */
    public void moveObject(Point p) {
        UCController.moveObjects(movingObject, p);

        repaint();
    }

    /*
     * Nabindovani klavesy Delete pro vymazani objektu
    */
    private void setKeyBinding() {
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "deleteObject");
        this.getActionMap().put("deleteObject", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (UCController.getSelectedItem() != null) {
                    JDialog.setDefaultLookAndFeelDecorated(true);
                    int response = JOptionPane.showConfirmDialog(null, "Do you want to delete selected item? Action can affect others diagrams.", "Confirm delete item",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (response == JOptionPane.YES_OPTION) {
                        UCController.deleteSelectedItem();
                        repaint();
                    }
                }
            }
        });
    }

    
    public ArrayList getObjects() {
        return UCController.getObjectList();
    }

    public UCController getUCController() {
        return UCController;
    }

    public void setUCController(UCController UCController) {
        this.UCController = UCController;
    }

}
