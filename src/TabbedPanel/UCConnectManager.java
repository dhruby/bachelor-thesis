/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package TabbedPanel;

import ToolBar.useCase.Actor;
import ToolBar.useCase.MainObject;
import ToolBar.useCase.UseCase;

/*
 * Trida pro kontrolu spojovacich hran u diagramu uziti
*/
public class UCConnectManager {

    public boolean canBeConnected(MainObject startObject, MainObject endObject, String lineType) {
        switch (lineType) {
            case "Generalization":
                if (startObject instanceof Actor && endObject instanceof Actor) {
                    return true;
                } else {
                    return false;
                }
            case "Association":
                if (startObject instanceof Actor && endObject instanceof Actor) {
                    return false;
                }else{
                    return true;
                }
            default:
                if (startObject instanceof UseCase && endObject instanceof UseCase) {
                    return true;
                }
        }

        return false;
    }
}
