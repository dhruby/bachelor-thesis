/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package TabbedPanel;

import Controllers.CDController;
import ToolBar.useCase.Line;
import ToolBar.useCase.MainObject;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import ToolBar.classDiagram.Class;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

/*
 * Zalozka diagram trid
 */
public class CDTab extends Tab {

    public static boolean drawLine = false;

    private CDController CDController;
    private CDTabMouseListener mouseListener;
    private Line drawingLine;
    private boolean drawline;
    private MainObject selectedItem;

    /*
     * Konstruktor
     */
    public CDTab(int index) {
        super(index);

        CDController = new CDController();
        mouseListener = new CDTabMouseListener(CDController, this);
        addMouseListener(mouseListener);
        addMouseMotionListener(new CDTabMouseMotionListener(mouseListener, this, CDController));
        //addKeyListener(new KeyLis());
        setKeyBinding();
    }

    /*
     * Funkce pro vykresleni zalozky
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        for (Line l : CDController.getLineList()) {
            l.draw(g);
        }

        for (MainObject object : CDController.getObjectList()) {
            object.draw(g);
        }

        if (drawingLine != null) {
            drawingLine.draw(g);
        }

        if (CDController.getSelectedItem() != null) {
            if (CDController.getSelectedItem() instanceof Line) {
                CDController.highlightLine(g, CDController.getSelectedItem());
            } else {
                CDController.highlightItem(g, CDController.getSelectedItem());

            }
        }
    }

    /*
    * Funkce pro posun objektu a jejich prekresleni
     */
    public void moveObject(Point p) {
        CDController.moveObjects(movingObject, p);

        repaint();
    }

    /*
     * Funkce pro nastaveni prave kreslici hrany
     */
    public void setDrawingLine(Line l) {
        drawingLine = l;
    }

    /*
    * Funkce pro zjisteni, zdali bylo kliknuto na objekt
    */
    protected Boolean isClickedOnObject(Point p) {
        for (MainObject object : CDController.getObjectList()) {
            if (object.isClickOnObject(p)) {
                return true;
            }
        }

        return false;
    }

    /*
     * Bindovani kliknuti klavesy Delete pro vymazani objektu
    */
    private void setKeyBinding() {
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "deleteObject");
        this.getActionMap().put("deleteObject", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (CDController.getSelectedItem() != null) {
                    JDialog.setDefaultLookAndFeelDecorated(true);
                    int response = JOptionPane.showConfirmDialog(null, "Do you want to delete selected item? Action can affect others diagrams.", "Confirm delete item",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (response == JOptionPane.YES_OPTION) {
                        CDController.deleteSelectedItem();
                        repaint();
                    }
                }
            }
        });
    }

    /*
     * Funkce pro nastaveni vybraneho objektu
    */
    public void setSelectedItem(MainObject object) {
        CDController.setSelectedItem(object);
    }

    /*
     * Funkce pro ziskani vybraneho objektu
    */
    public MainObject getSelectedItem() {
        return CDController.getSelectedItem();
    }

    /*
     * Funkce pro vygenerovani prislusnych trid pri zmene tabu
    */
    public void generateClasses(ArrayList<MainObject> objectList) {
        int i = 0;
        for (MainObject o : objectList) {
            if (!o.isIsGeneratedAssocObject()) {
                Class c = new Class(i * 40, 10, ((MainObject) o).getLabelText(), o);
                o.setAssocObject(c);
                o.setAssocObjectID(c.getId());
                o.setIsGeneratedAssocObject(true);

                CDController.getObjectList().add(c);

                c.setAssocObject(o);
                c.setAssocObjectID(o.getId());
            }
            i++;
        }
    }

    /*
     * Funkce pro kontrolu trid pri zmene tabu
    */
    public void controllClasses() {
        try {
            //ArrayList<MainObject> removableObjects = new ArrayList<>();
            for (Iterator< MainObject> it = CDController.getObjectList().iterator(); it.hasNext();) {
                MainObject o = it.next();
                if (o.getDelete()) {
                    o.setAssocObject(null);
                    o.setAssocObjectID(null);
                    CDController.deleteItem(o);
                }
            }
        } catch (ConcurrentModificationException e) {
        }
    }

    /*
     * Funkce pro ziskani controleru
    */
    public CDController getCDController() {
        return CDController;
    }

    /*
     * Funkce pro nastaveni controleru
    */
    public void setCDController(CDController CDController) {
        this.CDController = CDController;
    }

}
