/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package TabbedPanel;

import Controllers.PNController;
import ToolBar.OOPN.Place;
import ToolBar.OOPN.Transition;
import ToolBar.useCase.Line;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JOptionPane;

/*
 * Mouse listener pro panel OOPN
 */
public class PNTabMouseListener implements MouseListener {

    private PNController PNController;
    private PNTab tab;
    private Line drawingLine;
    private boolean drawLine;

    /*
     * Kostruktor
     */
    public PNTabMouseListener(PNController PNController, PNTab PNTab) {
        this.PNController = PNController;
        this.tab = PNTab;
    }

    /*
     * Funkce vyvolana pri kliknuti mysi
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        PNController.setSelectedMethodItem(null);
        // pokud se jedna do dvoj klik a kliknuto na objekt, 
        // vyvola se prislusny formular
        if (e.getClickCount() == 2) {
            if (PNController.getObjectByCoordinates(e.getPoint()) != null) {
                PNController.clickedOnObject(e.getPoint());
                tab.repaint();
            }
            PNController.setSelectedItem(null);

            // pokud je zmacknute tlacitko, prida se objekt
        } else if (tab.getActiveBtn() != "" && tab.getSelectedMethod() != null) {
            int x;
            int y = e.getY();
            if (e.getX() > tab.getDrawingBoardWidth() / 2) {
                x = e.getX() - (tab.getDrawingBoardWidth() / 2);
            } else {
                x = e.getX();
            }
            switch (tab.getActiveBtn()) {
                case "Place":
                    PNController.addObjectToMethodPlace(new Place(x, y));
                    break;
                case "Transition":
                    PNController.addObjectToMethodPlace(new Transition(x, y));
                    break;
            }
            // JOptionPane.showMessageDialog(tab, "You cannn add object only to method Petri net!", "Info", JOptionPane.INFORMATION_MESSAGE);
            PNController.setSelectedItem(null);
        } else if (tab.getActiveBtn() != "") {
            if (e.getPoint().x < tab.getDrawingBoardWidth() / 2) {
                switch (tab.getActiveBtn()) {
                    case "Place":
                        PNController.addObject(new Place(e.getX(), e.getY()));
                        break;
                    case "Transition":
                        PNController.addObject(new Transition(e.getX(), e.getY()));
                        break;
                }
            } else {
                JOptionPane.showMessageDialog(tab, "You don´t have choose method!", "Info", JOptionPane.INFORMATION_MESSAGE);
            }
            PNTab.drawLine = false;
            PNController.setSelectedItem(null);
        } else {
            PNController.setSelectedItem(PNController.getObjectByCoordinates(e.getPoint()));
            UCTab.drawLine = false;
        }
        tab.repaint();
    }

    /*
     * Funkce volana pri zmacknuti tlacitka mysi
     */
    @Override
    public void mousePressed(MouseEvent e
    ) {
        if (isActiveLine() && tab.isClickedOnObject(e.getPoint())) {
            drawingLine = new Line(tab.getActiveBtn());
            drawingLine.setStartX(e.getX());
            drawingLine.setStartY(e.getY());
            tab.setDrawingLine(drawingLine);
        } else if (tab.getActiveBtn() == "" && tab.isClickedOnObject(e.getPoint())) {
            tab.setMovingObject(PNController.getObjectByCoordinates(e.getPoint()));
            PNController.setSelectedItem(null);
        } else {
            drawingLine = null;
            tab.setDrawingLine(drawingLine);
        }
    }

    /*
     * Funkce volana pri uvolenni mysi
     */
    @Override
    public void mouseReleased(MouseEvent e
    ) {
        if (isActiveLine() && tab.isClickedOnObject(e.getPoint()) && drawingLine != null) {
            drawingLine.setEndX(e.getX());
            drawingLine.setEndY(e.getY());
            Point p = new Point((int) drawingLine.getStartX(), (int) drawingLine.getStartY());

            PNController.addLine(drawingLine);

            tab.setDrawingLine(new Line(tab.getActiveBtn()));

            tab.repaint();
        } else {
            drawingLine = null;
            tab.setDrawingLine(drawingLine);

            tab.repaint();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e
    ) {
    }

    @Override
    public void mouseExited(MouseEvent e
    ) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /*
     * Funkce pro zjisteni kresleni cary
     */
    protected boolean isActiveLine() {
        if (tab.getActiveBtn() == "Arc") {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Funkce pro nastaveni kreslici hrany
     */
    public void setDrawingLine(Point p) {
        if (drawingLine != null) {
            drawingLine.setEndX(p.x);
            drawingLine.setEndY(p.y);
            tab.setDrawingLine(drawingLine);
            tab.repaint();
        }
    }
}
