/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package TabbedPanel;

import Controllers.CDController;
import Controllers.UCController;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import ToolBar.classDiagram.Class;
import ToolBar.classDiagram.Interface;
import ToolBar.useCase.Line;
import java.awt.Point;

/*
 * Mouse listener pro diagram trid
*/
public class CDTabMouseListener implements MouseListener {

    private CDController CDController;
    private CDTab tab;
    private Line drawingLine;
    private boolean drawLine;

    /*
     * Konstruktor
    */
    public CDTabMouseListener(CDController CDController, CDTab CDTab) {
        this.CDController = CDController;
        this.tab = CDTab;
    }

    /*
     * Funkce vyvolana pri kliknuti mysi
    */
    @Override
    public void mouseClicked(MouseEvent e) {

        // pokud se jedna do dvoj klik a kliknuto na objekt, 
        // vyvola se prislusny formular
        if (e.getClickCount() == 2) {
            if (tab.isClickedOnObject(e.getPoint())) {
                CDController.clickedOnObject(e.getPoint());
            }
            CDController.setSelectedItem(null);
        } else if (tab.getActiveBtn() != "") { // pokud je zmacknute tlacitko, prida se objekt
            switch (tab.getActiveBtn()) {
                case "Class":
                    UCTab.drawLine = false;
                    CDController.addObject(new Class(e.getX(), e.getY()));
                    break;
                case "Interface":
                    UCTab.drawLine = false;
                    CDController.addObject(new Interface(e.getX(), e.getY()));
                    break;
                case "Attribute":
                    UCTab.drawLine = false;
                    CDController.attributeClick(e.getPoint());
                    break;
                case "Method":
                    UCTab.drawLine = false;
                    CDController.methodClick(e.getPoint());
                    break;
            }
            CDController.setSelectedItem(null);
        } else {
            CDController.setSelectedItem(CDController.getObjectByCoordinates(e.getPoint()));
            UCTab.drawLine = false;
        }
        tab.repaint();
    }

    /*
     * Funkce volana pri zmacknuti tlacitka mysi
    */
    @Override
    public void mousePressed(MouseEvent e) {
        // Pokud se ma vykrelit cara, vytvori a nastavi se jeji instance 
        if (isActiveLine() && tab.isClickedOnObject(e.getPoint())) {
            drawingLine = new Line(tab.getActiveBtn());
            drawingLine.setStartX(e.getX());
            drawingLine.setStartY(e.getY());
            tab.setDrawingLine(drawingLine);
            
            // pokud je kliknuto na objekt a neni zmacknute tlacitko, posune se objekt
        } else if (tab.getActiveBtn() == "" && tab.isClickedOnObject(e.getPoint())) {
            tab.setMovingObject(CDController.getObjectByCoordinates(e.getPoint()));
            CDController.setSelectedItem(null);
        } else {
            drawingLine = null;
            tab.setDrawingLine(drawingLine);
        }
    }

    /*
     * Funkce volana pri uvolenni mysi
    */
    @Override
    public void mouseReleased(MouseEvent e) {
        if (isActiveLine() && tab.isClickedOnObject(e.getPoint()) && drawingLine != null) {
            drawingLine.setEndX(e.getX());
            drawingLine.setEndY(e.getY());
            Point p = new Point((int) drawingLine.getStartX(), (int) drawingLine.getStartY());

            CDController.addLine(drawingLine);

            tab.setDrawingLine(new Line(tab.getActiveBtn()));

            tab.repaint();
        } else {
            drawingLine = null;
            tab.setDrawingLine(drawingLine);

            tab.repaint();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /*
     * Funkce pro zjisteni kresleni cary
    */
    protected boolean isActiveLine() {
        if (tab.getActiveBtn() == "Association"
                || tab.getActiveBtn() == "Generalization"
                || tab.getActiveBtn() == "Include"
                || tab.getActiveBtn() == "Extend") {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Funkce pro nastaveni kreslici hrany
    */
    public void setDrawingLine(Point p) {
        if (drawingLine != null) {
            drawingLine.setEndX(p.x);
            drawingLine.setEndY(p.y);
            tab.setDrawingLine(drawingLine);
            tab.repaint();
        }
    }

}
