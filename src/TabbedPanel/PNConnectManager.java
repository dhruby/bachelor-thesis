/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package TabbedPanel;

import ToolBar.useCase.MainObject;

/*
 * Trida pro kontrolu spravnosti spojovacich hran
*/
public class PNConnectManager {
    /*
     * Funkce pro kontrolu spravnosti spojovacich hran
    */
    public boolean canBeConnected(MainObject startObject, MainObject endObject){
        if(startObject.getClass() == endObject.getClass()){
            return false;
        }
        return true;
    }
}
