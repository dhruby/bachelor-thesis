/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package TabbedPanel;

import Controllers.PNController;
import ToolBar.useCase.Line;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import javafx.scene.paint.Stop;

/*
 * Mouse motion listener pro metodu OOPN
 */
public class PNTabMouseMotionListener implements MouseMotionListener {

    private PNTabMouseListener listener;
    private PNTab tab;
    private PNController PNController;
    private Line drawingLine;

    /*
     * Konstruktor
     */
    public PNTabMouseMotionListener(PNTabMouseListener listener, PNTab tab, PNController PNController) {
        this.listener = listener;
        this.tab = tab;
        this.PNController = PNController;
    }

    /*
     * Pokud je zmačnute prave tlacitko mysy a je tazeno, vyvola se tato funkce
     * bud nakresli caru nebo posune objektem
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        if (listener.isActiveLine()) {
            listener.setDrawingLine(e.getPoint());
            tab.repaint();
        } else if (tab.getActiveBtn() == "" && tab.isClickedOnObject(e.getPoint())) {
            tab.moveObject(e.getPoint());
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }
}
