/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package xmlParsing;

import ToolBar.OOPN.Place;
import ToolBar.OOPN.Transition;
import ToolBar.classDiagram.Attribute;
import ToolBar.useCase.Actor;
import ToolBar.useCase.Line;
import ToolBar.useCase.MainObject;
import ToolBar.useCase.UseCase;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import ToolBar.classDiagram.Class;
import ToolBar.classDiagram.Interface;
import ToolBar.classDiagram.Method;
import java.io.IOException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import TabbedPanel.TabbedPanel;
import java.util.UUID;

/*
 * Trida pro parsovani xml dokumentu
*/
public class XMLParser {

    /*
     * Funkce pro ulozeni projektu
    */
    public void save(File file, ArrayList<MainObject> ucList, ArrayList<Line> ucLineList,
            ArrayList<MainObject> cdList, ArrayList<Line> cdLineList,
            ArrayList<MainObject> pnList, ArrayList<Line> pnLineList)
            throws ParserConfigurationException, TransformerConfigurationException, TransformerException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = (Document) dBuilder.newDocument();

        Element rootElement;
        rootElement = doc.createElement("root");
        doc.appendChild(rootElement);

        Element useCase = doc.createElement("UseCaseDiagram");
        parseUseCase(useCase, doc, ucList, ucLineList);
        rootElement.appendChild(useCase);

        Element classDiagram = doc.createElement("ClassDiagram");
        parseClassDiagram(classDiagram, doc, cdList, cdLineList);
        rootElement.appendChild(classDiagram);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(file);
        transformer.transform(source, result);
    }

    /*
     * Funkce pro vytvoreni xml z use case objektu
    */
    private void parseUseCase(Element uc, Document doc, ArrayList<MainObject> ucList, ArrayList<Line> ucLineList) {

        Element actors = doc.createElement("Actors");
        for (MainObject o : ucList) {
            if (o instanceof Actor) {
                Element actor = doc.createElement("Actor");

                Element id = doc.createElement("ID");
                id.appendChild(doc.createTextNode(o.getId().toString()));

                Element assocId = doc.createElement("AssocObjectId");
                String sAssocId = o.getAssocObjectID() == null ? "" : o.getAssocObjectID().toString();
                assocId.appendChild(doc.createTextNode(sAssocId));

                Element x = doc.createElement("X");
                x.appendChild(doc.createTextNode(String.valueOf(o.getX())));

                Element y = doc.createElement("Y");
                y.appendChild(doc.createTextNode(String.valueOf(o.getY())));

                Element width = doc.createElement("Width");
                width.appendChild(doc.createTextNode(String.valueOf(String.valueOf(o.getWidth()))));

                Element height = doc.createElement("Height");
                height.appendChild(doc.createTextNode(String.valueOf(String.valueOf(o.getHeight()))));

                Element label = doc.createElement("Label");
                label.appendChild(doc.createTextNode(o.getLabelText()));

                //TODO odkazy na pridruzene objekty
                String sDelete = o.getDelete() ? "True" : "False";
                Element delete = doc.createElement("Delete");
                delete.appendChild(doc.createTextNode(sDelete));

                String sIsGenerateAssocObject = o.isIsGeneratedAssocObject() ? "True" : "False";
                Element isGenerateAssocObject = doc.createElement("IsGeneratedAssocObject");
                isGenerateAssocObject.appendChild(doc.createTextNode(sIsGenerateAssocObject));

                actor.appendChild(id);
                actor.appendChild(assocId);
                actor.appendChild(x);
                actor.appendChild(y);
                actor.appendChild(width);
                actor.appendChild(height);
                actor.appendChild(label);
                actor.appendChild(delete);
                actor.appendChild(isGenerateAssocObject);

                actors.appendChild(actor);
            }
        }

        uc.appendChild(actors);

        Element useCases = doc.createElement("UseCases");
        for (MainObject o : ucList) {
            if (o instanceof UseCase) {
                Element useCase = doc.createElement("UseCase");

                Element id = doc.createElement("ID");
                id.appendChild(doc.createTextNode(o.getId().toString()));

                Element assocId = doc.createElement("AssocObjectId");
                String sAssocId = o.getAssocObjectID() == null ? "" : o.getAssocObjectID().toString();
                assocId.appendChild(doc.createTextNode(sAssocId));

                Element x = doc.createElement("X");
                x.appendChild(doc.createTextNode(String.valueOf(o.getX())));

                Element y = doc.createElement("Y");
                y.appendChild(doc.createTextNode(String.valueOf(o.getY())));

                Element width = doc.createElement("Width");
                width.appendChild(doc.createTextNode(String.valueOf(String.valueOf(o.getWidth()))));

                Element height = doc.createElement("Height");
                height.appendChild(doc.createTextNode(String.valueOf(String.valueOf(o.getHeight()))));

                Element label = doc.createElement("Label");
                label.appendChild(doc.createTextNode(o.getLabelText()));

                //TODO odkazy na pridruzene objekty
                String sDelete = o.getDelete() ? "True" : "False";
                Element delete = doc.createElement("Delete");
                delete.appendChild(doc.createTextNode(sDelete));

                String sIsGenerateAssocObject = o.isIsGeneratedAssocObject() ? "True" : "False";
                Element isGeneratedAssocObject = doc.createElement("IsGeneratedAssocObject");
                isGeneratedAssocObject.appendChild(doc.createTextNode(sIsGenerateAssocObject));

                useCase.appendChild(id);
                useCase.appendChild(assocId);
                useCase.appendChild(x);
                useCase.appendChild(y);
                useCase.appendChild(width);
                useCase.appendChild(height);
                useCase.appendChild(label);
                useCase.appendChild(delete);
                useCase.appendChild(isGeneratedAssocObject);

                useCases.appendChild(useCase);
            }
        }

        uc.appendChild(useCases);

        Element lines = doc.createElement("Lines");

        for (Line l : ucLineList) {
            Element line = doc.createElement("Line");

            Element id = doc.createElement("ID");
            id.appendChild(doc.createTextNode(l.getId().toString()));

            Element startObjectID = doc.createElement("StartObjectID");
            startObjectID.appendChild(doc.createTextNode(l.getStartObject().getId().toString()));

            Element endtObjectID = doc.createElement("EndObjectID");
            endtObjectID.appendChild(doc.createTextNode(l.getEndObject().getId().toString()));

            Element type = doc.createElement("Type");
            type.appendChild(doc.createTextNode(l.getType()));

            Element startX = doc.createElement("StartX");
            startX.appendChild(doc.createTextNode(String.valueOf(l.getStartX())));

            Element startY = doc.createElement("StartY");
            startY.appendChild(doc.createTextNode(String.valueOf(String.valueOf(l.getStartY()))));

            Element endX = doc.createElement("EndX");
            endX.appendChild(doc.createTextNode(String.valueOf(String.valueOf(l.getEndX()))));

            Element endY = doc.createElement("EndY");
            endY.appendChild(doc.createTextNode(String.valueOf(String.valueOf(l.getEndY()))));

            //TODO odkazy na pridruzene objekty
            line.appendChild(id);
            line.appendChild(startObjectID);
            line.appendChild(endtObjectID);
            line.appendChild(type);
            line.appendChild(startX);
            line.appendChild(startY);
            line.appendChild(endX);
            line.appendChild(endY);

            lines.appendChild(line);
        }

        uc.appendChild(lines);
    }

    /*
     * Funkce pro vytvoreni xml z class diagram objektu
    */
    private void parseClassDiagram(Element cd, Document doc, ArrayList<MainObject> cdList, ArrayList<Line> cdLineList) {

        Element classes = doc.createElement("Classes");
        for (MainObject o : cdList) {
            if (o instanceof Class && !(o instanceof Interface)) {
                Element c = doc.createElement("Class");

                Element id = doc.createElement("ID");
                id.appendChild(doc.createTextNode(o.getId().toString()));

                Element assocId = doc.createElement("AssocObjectId");
                String sAssocId = o.getAssocObjectID() == null ? "" : o.getAssocObjectID().toString();
                assocId.appendChild(doc.createTextNode(sAssocId));

                Element x = doc.createElement("X");
                x.appendChild(doc.createTextNode(String.valueOf(o.getX())));

                Element y = doc.createElement("Y");
                y.appendChild(doc.createTextNode(String.valueOf(o.getY())));

                Element width = doc.createElement("Width");
                width.appendChild(doc.createTextNode(String.valueOf(String.valueOf(o.getWidth()))));

                Element height = doc.createElement("Height");
                height.appendChild(doc.createTextNode(String.valueOf(String.valueOf(o.getHeight()))));

                Element label = doc.createElement("Label");
                label.appendChild(doc.createTextNode(o.getLabelText()));

                String sDelete = o.getDelete() ? "True" : "False";
                Element delete = doc.createElement("Delete");
                delete.appendChild(doc.createTextNode(sDelete));

                String sIsGeneratedAssocObject = o.isIsGeneratedAssocObject() ? "True" : "False";
                Element isGeneratedAssocObject = doc.createElement("IsGeneratedAssocObject");
                isGeneratedAssocObject.appendChild(doc.createTextNode(sIsGeneratedAssocObject));

                //TODO odkazy na pridruzene objekty
                Element attributes = doc.createElement("Attributes");
                for (Attribute a : ((Class) o).getAttributeList()) {
                    Element attribute = doc.createElement("Attribute");

                    Element idA = doc.createElement("ID");
                    idA.appendChild(doc.createTextNode(a.getId().toString()));

                    Element assocIdA = doc.createElement("AssocObjectId");
                    String sAssocIdA = a.getAssocObjectID() == null ? "" : a.getAssocObjectID().toString();
                    assocIdA.appendChild(doc.createTextNode(sAssocIdA));

                    Element access = doc.createElement("Access");
                    access.appendChild(doc.createTextNode(a.getAccess()));

                    Element name = doc.createElement("Name");
                    name.appendChild(doc.createTextNode(a.getName()));

                    Element dataType = doc.createElement("DataType");
                    dataType.appendChild(doc.createTextNode(a.getDataType()));

                    String sDeleteA = a.getDelete() ? "True" : "False";
                    Element deleteA = doc.createElement("Delete");
                    deleteA.appendChild(doc.createTextNode(sDeleteA));

                    String sIsGeneratedAssocObjectA = a.isIsGeneratedAssocObject() ? "True" : "False";
                    Element isGeneratedAssocObjectA = doc.createElement("IsGeneratedAssocObject");
                    isGeneratedAssocObjectA.appendChild(doc.createTextNode(sIsGeneratedAssocObjectA));

                    attribute.appendChild(idA);
                    attribute.appendChild(assocIdA);
                    attribute.appendChild(access);
                    attribute.appendChild(name);
                    attribute.appendChild(dataType);
                    attribute.appendChild(deleteA);
                    attribute.appendChild(isGeneratedAssocObjectA);

                    attributes.appendChild(attribute);
                }

                Element methods = doc.createElement("Methods");
                for (Method m : ((Class) o).getMethodList()) {
                    Element method = doc.createElement("Method");

                    Element idM = doc.createElement("ID");
                    idM.appendChild(doc.createTextNode(m.getId().toString()));

                    Element assocIdM = doc.createElement("AssocObjectId");
                    String sAssocIdM = m.getAssocObjectID() == null ? "" : m.getAssocObjectID().toString();
                    assocIdM.appendChild(doc.createTextNode(sAssocIdM));

                    Element access = doc.createElement("Access");
                    access.appendChild(doc.createTextNode(m.getAccess()));

                    Element name = doc.createElement("Name");
                    name.appendChild(doc.createTextNode(m.getName()));

                    Element dataType = doc.createElement("DataType");
                    dataType.appendChild(doc.createTextNode(m.getDataType()));

                    String sDeleteM = m.getDelete() ? "True" : "False";
                    Element deleteM = doc.createElement("Delete");
                    deleteM.appendChild(doc.createTextNode(sDeleteM));

                    String sIsGeneratedAssocObjectM = m.isIsGeneratedAssocObject() ? "True" : "False";
                    Element isGeneratedAssocObjectM = doc.createElement("IsGeneratedAssocObject");
                    isGeneratedAssocObjectM.appendChild(doc.createTextNode(sIsGeneratedAssocObjectM));

                    method.appendChild(idM);
                    method.appendChild(assocIdM);
                    method.appendChild(access);
                    method.appendChild(name);
                    method.appendChild(dataType);
                    method.appendChild(deleteM);
                    method.appendChild(isGeneratedAssocObjectM);

                    methods.appendChild(method);
                }

                Element petriNet = doc.createElement("PetriNet");
                parsePN(petriNet, doc, ((Class) o).getPnList(), ((Class) o).getPnLineList());

                c.appendChild(id);
                c.appendChild(assocId);
                c.appendChild(x);
                c.appendChild(y);
                c.appendChild(width);
                c.appendChild(height);
                c.appendChild(label);
                c.appendChild(delete);
                c.appendChild(isGeneratedAssocObject);

                c.appendChild(attributes);
                c.appendChild(methods);
                c.appendChild(petriNet);

                classes.appendChild(c);
            }
        }

        Element interfaces = doc.createElement("Interfaces");
        for (MainObject o : cdList) {
            if (o instanceof Interface) {
                Element i = doc.createElement("Interface");

                Element id = doc.createElement("ID");
                id.appendChild(doc.createTextNode(o.getId().toString()));

                Element x = doc.createElement("X");
                x.appendChild(doc.createTextNode(String.valueOf(o.getX())));

                Element y = doc.createElement("Y");
                y.appendChild(doc.createTextNode(String.valueOf(o.getY())));

                Element width = doc.createElement("Width");
                width.appendChild(doc.createTextNode(String.valueOf(String.valueOf(o.getWidth()))));

                Element height = doc.createElement("Height");
                height.appendChild(doc.createTextNode(String.valueOf(String.valueOf(o.getHeight()))));

                Element label = doc.createElement("Label");
                label.appendChild(doc.createTextNode(o.getLabelText()));

                //TODO odkazy na pridruzene objekty
                String sDelete = o.getDelete() ? "True" : "False";
                Element delete = doc.createElement("Delete");
                delete.appendChild(doc.createTextNode(sDelete));

                Element attributes = doc.createElement("Attributes");
                for (Attribute a : ((Interface) o).getAttributeList()) {
                    Element attribute = doc.createElement("Attribute");

                    Element idA = doc.createElement("ID");
                    idA.appendChild(doc.createTextNode(a.getId().toString()));

                    Element access = doc.createElement("Access");
                    access.appendChild(doc.createTextNode(a.getAccess()));

                    Element name = doc.createElement("Name");
                    name.appendChild(doc.createTextNode(a.getName()));

                    Element dataType = doc.createElement("DataType");
                    dataType.appendChild(doc.createTextNode(a.getDataType()));

                    attribute.appendChild(idA);
                    attribute.appendChild(access);
                    attribute.appendChild(name);
                    attribute.appendChild(dataType);

                    attributes.appendChild(attribute);
                }

                Element methods = doc.createElement("Methods");
                for (Method m : ((Interface) o).getMethodList()) {
                    Element method = doc.createElement("Method");

                    Element idM = doc.createElement("ID");
                    idM.appendChild(doc.createTextNode(m.getId().toString()));

                    Element access = doc.createElement("Access");
                    access.appendChild(doc.createTextNode(m.getAccess()));

                    Element name = doc.createElement("Name");
                    name.appendChild(doc.createTextNode(m.getName()));

                    Element dataType = doc.createElement("DataType");
                    dataType.appendChild(doc.createTextNode(m.getDataType()));

                    method.appendChild(idM);
                    method.appendChild(access);
                    method.appendChild(name);
                    method.appendChild(dataType);

                    methods.appendChild(method);
                }

                i.appendChild(id);
                i.appendChild(x);
                i.appendChild(y);
                i.appendChild(width);
                i.appendChild(height);
                i.appendChild(label);
                i.appendChild(delete);

                i.appendChild(attributes);
                i.appendChild(methods);

                interfaces.appendChild(i);
            }
        }

        Element lines = doc.createElement("Lines");

        for (Line l : cdLineList) {
            Element line = doc.createElement("Line");

            Element idL = doc.createElement("ID");
            idL.appendChild(doc.createTextNode(l.getId().toString()));

            Element startObjectID = doc.createElement("StartObjectID");
            startObjectID.appendChild(doc.createTextNode(l.getStartObject().getId().toString()));

            Element endtObjectID = doc.createElement("EndObjectID");
            endtObjectID.appendChild(doc.createTextNode(l.getEndObject().getId().toString()));

            Element type = doc.createElement("Type");
            type.appendChild(doc.createTextNode(l.getType()));

            Element startX = doc.createElement("StartX");
            startX.appendChild(doc.createTextNode(String.valueOf(l.getStartX())));

            Element startY = doc.createElement("StartY");
            startY.appendChild(doc.createTextNode(String.valueOf(String.valueOf(l.getStartY()))));

            Element endX = doc.createElement("EndX");
            endX.appendChild(doc.createTextNode(String.valueOf(String.valueOf(l.getEndX()))));

            Element endY = doc.createElement("EndY");
            endY.appendChild(doc.createTextNode(String.valueOf(String.valueOf(l.getEndY()))));

            //TODO odkazy na pridruzene objekty
            line.appendChild(idL);
            line.appendChild(startObjectID);
            line.appendChild(endtObjectID);
            line.appendChild(type);
            line.appendChild(startX);
            line.appendChild(startY);
            line.appendChild(endX);
            line.appendChild(endY);

            lines.appendChild(line);
        }

        cd.appendChild(classes);
        cd.appendChild(interfaces);
        cd.appendChild(lines);
    }

    /*
     * Funkce pro vytvoreni xml z Petriho site
    */
    private void parsePN(Element pn, Document doc, ArrayList<MainObject> pnList, ArrayList<Line> pnLineList) {

        Element places = doc.createElement("Places");
        for (MainObject o : pnList) {
            if (o instanceof Place) {
                Element place = doc.createElement("Place");

                Element id = doc.createElement("ID");
                id.appendChild(doc.createTextNode(o.getId().toString()));

                Element assocId = doc.createElement("AssocObjectId");
                String sAssocId = o.getAssocObjectID() == null ? "" : o.getAssocObjectID().toString();
                assocId.appendChild(doc.createTextNode(sAssocId));

                Element x = doc.createElement("X");
                x.appendChild(doc.createTextNode(String.valueOf(o.getX())));

                Element y = doc.createElement("Y");
                y.appendChild(doc.createTextNode(String.valueOf(o.getY())));

                Element width = doc.createElement("Width");
                width.appendChild(doc.createTextNode(String.valueOf(String.valueOf(o.getWidth()))));

                Element height = doc.createElement("Height");
                height.appendChild(doc.createTextNode(String.valueOf(String.valueOf(o.getHeight()))));

                Element label = doc.createElement("Label");
                label.appendChild(doc.createTextNode(o.getLabelText()));

                //TODO odkazy na pridruzene objekty
                String sDelete = o.getDelete() ? "True" : "False";
                Element delete = doc.createElement("Delete");
                delete.appendChild(doc.createTextNode(sDelete));

                place.appendChild(id);
                place.appendChild(assocId);
                place.appendChild(x);
                place.appendChild(y);
                place.appendChild(width);
                place.appendChild(height);
                place.appendChild(label);
                place.appendChild(delete);

                if (((Place) o).getPnList().size() > 0) {
                    Element petriNet = doc.createElement("PetriNet");
                    parsePN(petriNet, doc, ((Place) o).getPnList(), ((Place) o).getPnLineList());
                    place.appendChild(petriNet);
                }

                places.appendChild(place);
            }
        }

        Element transitions = doc.createElement("Transitions");
        for (MainObject o : pnList) {
            if (o instanceof Transition) {
                Element transition = doc.createElement("Transition");

                Element id = doc.createElement("ID");
                id.appendChild(doc.createTextNode(o.getId().toString()));

                Element x = doc.createElement("X");
                x.appendChild(doc.createTextNode(String.valueOf(o.getX())));

                Element y = doc.createElement("Y");
                y.appendChild(doc.createTextNode(String.valueOf(o.getY())));

                Element width = doc.createElement("Width");
                width.appendChild(doc.createTextNode(String.valueOf(String.valueOf(o.getWidth()))));

                Element height = doc.createElement("Height");
                height.appendChild(doc.createTextNode(String.valueOf(String.valueOf(o.getHeight()))));

                Element label = doc.createElement("Label");
                label.appendChild(doc.createTextNode(o.getLabelText()));

                //TODO odkazy na pridruzene objekty
                String sDelete = o.getDelete() ? "True" : "False";
                Element delete = doc.createElement("Delete");
                delete.appendChild(doc.createTextNode(sDelete));

                Element guard = doc.createElement("Guard");
                guard.appendChild(doc.createTextNode(((Transition) o).getGuard()));

                Element action = doc.createElement("Action");
                action.appendChild(doc.createTextNode(((Transition) o).getAction()));

                transition.appendChild(id);
                transition.appendChild(x);
                transition.appendChild(y);
                transition.appendChild(width);
                transition.appendChild(height);
                transition.appendChild(label);
                transition.appendChild(delete);
                transition.appendChild(guard);
                transition.appendChild(action);

                transitions.appendChild(transition);
            }
        }

        Element lines = doc.createElement("Lines");

        for (Line l : pnLineList) {
            Element line = doc.createElement("Line");

            Element id = doc.createElement("ID");
            id.appendChild(doc.createTextNode(l.getId().toString()));

            Element startObjectID = doc.createElement("StartObjectID");
            startObjectID.appendChild(doc.createTextNode(l.getStartObject().getId().toString()));

            Element endtObjectID = doc.createElement("EndObjectID");
            endtObjectID.appendChild(doc.createTextNode(l.getEndObject().getId().toString()));

            Element type = doc.createElement("Type");
            type.appendChild(doc.createTextNode(l.getType()));

            Element startX = doc.createElement("StartX");
            startX.appendChild(doc.createTextNode(String.valueOf(l.getStartX())));

            Element startY = doc.createElement("StartY");
            startY.appendChild(doc.createTextNode(String.valueOf(String.valueOf(l.getStartY()))));

            Element endX = doc.createElement("EndX");
            endX.appendChild(doc.createTextNode(String.valueOf(String.valueOf(l.getEndX()))));

            Element endY = doc.createElement("EndY");
            endY.appendChild(doc.createTextNode(String.valueOf(String.valueOf(l.getEndY()))));

            //TODO odkazy na pridruzene objekty
            line.appendChild(id);
            line.appendChild(startObjectID);
            line.appendChild(endtObjectID);
            line.appendChild(type);
            line.appendChild(startX);
            line.appendChild(startY);
            line.appendChild(endX);
            line.appendChild(endY);

            lines.appendChild(line);
        }

        pn.appendChild(places);
        pn.appendChild(transitions);
        pn.appendChild(lines);
    }

    /*
     * Nacteni projektu
    */
    public void load(File file, TabbedPanel tabbedPanel) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory dbFactory
                = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(file);
        doc.getDocumentElement().normalize();

        tabbedPanel.getUseCasePanel().getUCController().setObjectList(loadUseCaseObjects(doc));
        tabbedPanel.getUseCasePanel().getUCController().setLineList(loadUseCaseLines(doc));

        tabbedPanel.getClassDiagramPanel().getCDController().setObjectList(loadClassDiagramObjects(doc));
        tabbedPanel.getClassDiagramPanel().getCDController().setLineList(loadClassDiagramLines(doc));
    }

    /*
     * Funkce pro nacteni objektu duagramu uziti
    */
    private ArrayList<MainObject> loadUseCaseObjects(Document doc) {
        ArrayList<MainObject> useCaseObjects = new ArrayList<MainObject>();
        NodeList nListActors = doc.getElementsByTagName("Actor");
        for (int temp = 0; temp < nListActors.getLength(); temp++) {
            Node nNode = nListActors.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;

                String id = eElement
                        .getElementsByTagName("ID")
                        .item(0)
                        .getTextContent();

                Actor a = new Actor(UUID.fromString(id));

                a.setX(Integer.parseInt(eElement
                        .getElementsByTagName("X")
                        .item(0)
                        .getTextContent()));
                a.setY(Integer.parseInt(eElement
                        .getElementsByTagName("Y")
                        .item(0)
                        .getTextContent()));
                a.setWidth(Integer.parseInt(eElement
                        .getElementsByTagName("Width")
                        .item(0)
                        .getTextContent()));
                a.setHeight(Integer.parseInt(eElement
                        .getElementsByTagName("Height")
                        .item(0)
                        .getTextContent()));
                a.setLabel(eElement
                        .getElementsByTagName("Label")
                        .item(0)
                        .getTextContent());
                String sDelete = eElement
                        .getElementsByTagName("Delete")
                        .item(0)
                        .getTextContent();
                boolean delete = sDelete.equals("True") ? true : false;
                a.setDelete(delete);

                String sIsGeneratedAssocObject = eElement
                        .getElementsByTagName("IsGeneratedAssocObject")
                        .item(0)
                        .getTextContent();
                boolean isGeneratedAssocObject = sIsGeneratedAssocObject.equals("True") ? true : false;
                a.setIsGeneratedAssocObject(isGeneratedAssocObject);

                String assocID = eElement
                        .getElementsByTagName("AssocObjectId")
                        .item(0)
                        .getTextContent();

                if (!assocID.equals("")) {
                    a.setAssocObjectID(UUID.fromString(assocID));
                }

                useCaseObjects.add(a);
            }
        }

        NodeList nListUseCases = doc.getElementsByTagName("UseCase");
        for (int temp = 0; temp < nListUseCases.getLength(); temp++) {
            Node nNode = nListUseCases.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                String id = eElement
                        .getElementsByTagName("ID")
                        .item(0)
                        .getTextContent();

                UseCase uc = new UseCase(UUID.fromString(id));

                uc.setX(Integer.parseInt(eElement
                        .getElementsByTagName("X")
                        .item(0)
                        .getTextContent()));
                uc.setY(Integer.parseInt(eElement
                        .getElementsByTagName("Y")
                        .item(0)
                        .getTextContent()));
                uc.setWidth(Integer.parseInt(eElement
                        .getElementsByTagName("Width")
                        .item(0)
                        .getTextContent()));
                uc.setHeight(Integer.parseInt(eElement
                        .getElementsByTagName("Height")
                        .item(0)
                        .getTextContent()));
                uc.setLabel(eElement
                        .getElementsByTagName("Label")
                        .item(0)
                        .getTextContent());
                String sDelete = eElement
                        .getElementsByTagName("Delete")
                        .item(0)
                        .getTextContent();
                boolean delete = sDelete.equals("True") ? true : false;
                uc.setDelete(delete);

                String sIsGeneratedAssocObject = eElement
                        .getElementsByTagName("IsGeneratedAssocObject")
                        .item(0)
                        .getTextContent();
                boolean isGeneratedAssocObject = sIsGeneratedAssocObject.equals("True") ? true : false;
                uc.setIsGeneratedAssocObject(isGeneratedAssocObject);

                String assocID = eElement
                        .getElementsByTagName("AssocObjectId")
                        .item(0)
                        .getTextContent();
                if (!assocID.equals("")) {
                    uc.setAssocObjectID(UUID.fromString(assocID));
                }

                uc.createEllipse();

                useCaseObjects.add(uc);
            }
        }

        return useCaseObjects;
    }

    /*
     * Funkce pro nacteni spojovacich hran pripadu uziti
    */
    private ArrayList<Line> loadUseCaseLines(Document doc) {
        ArrayList<Line> lineList = new ArrayList<Line>();
        NodeList nUseCase = doc.getElementsByTagName("UseCaseDiagram");
        Node nNodeUC = nUseCase.item(0);
        NodeList nListUseCaseLines = ((Element) nNodeUC).getElementsByTagName("Line");
        for (int temp = 0; temp < nListUseCaseLines.getLength(); temp++) {
            Node nNode = nListUseCaseLines.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;

                String id = eElement
                        .getElementsByTagName("ID")
                        .item(0)
                        .getTextContent();

                Line l = new Line(UUID.fromString(id));

                l.setStartObjectID(UUID.fromString(eElement
                        .getElementsByTagName("StartObjectID")
                        .item(0)
                        .getTextContent()));

                l.setEndObjectID(UUID.fromString(eElement
                        .getElementsByTagName("EndObjectID")
                        .item(0)
                        .getTextContent()));

                l.setType(eElement
                        .getElementsByTagName("Type")
                        .item(0)
                        .getTextContent());
                l.setStartX(Integer.parseInt(eElement
                        .getElementsByTagName("StartX")
                        .item(0)
                        .getTextContent()));
                l.setStartY(Integer.parseInt(eElement
                        .getElementsByTagName("StartY")
                        .item(0)
                        .getTextContent()));
                l.setEndX(Integer.parseInt(eElement
                        .getElementsByTagName("EndX")
                        .item(0)
                        .getTextContent()));
                l.setEndY(Integer.parseInt(eElement
                        .getElementsByTagName("EndY")
                        .item(0)
                        .getTextContent()));

                lineList.add(l);
            }
        }

        return lineList;
    }

    /*
     * Funce pro nacteni objektu diagramu trid
    */
    private ArrayList<MainObject> loadClassDiagramObjects(Document doc) {
        ArrayList<MainObject> classObjects = new ArrayList<MainObject>();
        NodeList nListClasses = doc.getElementsByTagName("Class");
        for (int temp = 0; temp < nListClasses.getLength(); temp++) {
            Node nNode = nListClasses.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;

                String id = eElement
                        .getElementsByTagName("ID")
                        .item(0)
                        .getTextContent();

                Class c = new Class(UUID.fromString(id));

                c.setX(Integer.parseInt(eElement
                        .getElementsByTagName("X")
                        .item(0)
                        .getTextContent()));
                c.setY(Integer.parseInt(eElement
                        .getElementsByTagName("Y")
                        .item(0)
                        .getTextContent()));
                c.setWidth(Integer.parseInt(eElement
                        .getElementsByTagName("Width")
                        .item(0)
                        .getTextContent()));
                c.setHeight(Integer.parseInt(eElement
                        .getElementsByTagName("Height")
                        .item(0)
                        .getTextContent()));
                c.setLabel(eElement
                        .getElementsByTagName("Label")
                        .item(0)
                        .getTextContent());
                String sDelete = eElement
                        .getElementsByTagName("Delete")
                        .item(0)
                        .getTextContent();
                boolean delete = sDelete.equals("True") ? true : false;
                c.setDelete(delete);

                String sIsGeneratedAssocObject = eElement
                        .getElementsByTagName("IsGeneratedAssocObject")
                        .item(0)
                        .getTextContent();
                boolean isGeneratedAssocObject = sIsGeneratedAssocObject.equals("True") ? true : false;
                c.setIsGeneratedAssocObject(isGeneratedAssocObject);

                String assocID = eElement
                        .getElementsByTagName("AssocObjectId")
                        .item(0)
                        .getTextContent();
                if (!assocID.equals("")) {
                    c.setAssocObjectID(UUID.fromString(assocID));
                }

                NodeList nListAttributes = eElement.getElementsByTagName("Attribute");
                for (int i = 0; i < nListAttributes.getLength(); i++) {
                    Node nAttributesNode = nListAttributes.item(i);
                    if (nAttributesNode.getNodeType() == nAttributesNode.ELEMENT_NODE) {
                        Element eAttribut = (Element) nAttributesNode;
                        UUID idA = UUID.fromString(eAttribut
                                .getElementsByTagName("ID")
                                .item(0)
                                .getTextContent());
                        Attribute a = new Attribute(idA,
                                eAttribut
                                .getElementsByTagName("Access")
                                .item(0)
                                .getTextContent(),
                                eAttribut
                                .getElementsByTagName("Name")
                                .item(0)
                                .getTextContent(),
                                eAttribut
                                .getElementsByTagName("DataType")
                                .item(0)
                                .getTextContent());

                        String assocIDA = eAttribut
                                .getElementsByTagName("AssocObjectId")
                                .item(0)
                                .getTextContent();
                        if (!assocIDA.equals("")) {
                            a.setAssocObjectID(UUID.fromString(assocIDA));
                        }

                        String sIsGeneratedAssocObjectA = eAttribut
                                .getElementsByTagName("IsGeneratedAssocObject")
                                .item(0)
                                .getTextContent();
                        boolean isGeneratedAssocObjectA = sIsGeneratedAssocObjectA.equals("True") ? true : false;
                        a.setIsGeneratedAssocObject(isGeneratedAssocObjectA);

                        c.addAttribute(a);
                    }
                }

                NodeList nListMethods = eElement.getElementsByTagName("Method");
                for (int i = 0; i < nListMethods.getLength(); i++) {
                    Node nMethodsNode = nListMethods.item(i);
                    if (nMethodsNode.getNodeType() == nMethodsNode.ELEMENT_NODE) {
                        Element eMethod = (Element) nMethodsNode;
                        UUID idM = UUID.fromString(eMethod
                                .getElementsByTagName("ID")
                                .item(0)
                                .getTextContent());
                        Method m = new Method(idM,
                                eMethod
                                .getElementsByTagName("Access")
                                .item(0)
                                .getTextContent(),
                                eMethod
                                .getElementsByTagName("Name")
                                .item(0)
                                .getTextContent(),
                                eMethod
                                .getElementsByTagName("DataType")
                                .item(0)
                                .getTextContent());
                        String assocIDM = eMethod
                                .getElementsByTagName("AssocObjectId")
                                .item(0)
                                .getTextContent();
                        if (!assocIDM.equals("")) {
                            m.setAssocObjectID(UUID.fromString(assocIDM));
                        }

                        String sIsGeneratedAssocObjectM = eMethod
                                .getElementsByTagName("IsGeneratedAssocObject")
                                .item(0)
                                .getTextContent();
                        boolean isGeneratedAssocObjectM = sIsGeneratedAssocObjectM.equals("True") ? true : false;
                        m.setIsGeneratedAssocObject(isGeneratedAssocObjectM);

                        c.addMethod(m);
                    }
                }

                c.setPnList(loadPetriNet(eElement, "Class"));
                c.setPnLineList(loadPetriNetLines(eElement, "Class"));
                classObjects.add(c);
            }
        }

        NodeList nListInterfaces = doc.getElementsByTagName("Interface");
        for (int temp = 0; temp < nListInterfaces.getLength(); temp++) {
            Node nNode = nListInterfaces.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;

                String id = eElement
                        .getElementsByTagName("ID")
                        .item(0)
                        .getTextContent();

                Interface i = new Interface(UUID.fromString(id));

                i.setX(Integer.parseInt(eElement
                        .getElementsByTagName("X")
                        .item(0)
                        .getTextContent()));
                i.setY(Integer.parseInt(eElement
                        .getElementsByTagName("Y")
                        .item(0)
                        .getTextContent()));
                i.setWidth(Integer.parseInt(eElement
                        .getElementsByTagName("Width")
                        .item(0)
                        .getTextContent()));
                i.setHeight(Integer.parseInt(eElement
                        .getElementsByTagName("Height")
                        .item(0)
                        .getTextContent()));
                i.setLabel(eElement
                        .getElementsByTagName("Label")
                        .item(0)
                        .getTextContent());
                String sDelete = eElement
                        .getElementsByTagName("Delete")
                        .item(0)
                        .getTextContent();
                boolean delete = sDelete.equals("True") ? true : false;
                i.setDelete(delete);

                NodeList nListAttributes = eElement.getElementsByTagName("Attribute");
                for (int k = 0; k < nListAttributes.getLength(); k++) {
                    Node nAttributesNode = nListAttributes.item(k);
                    if (nAttributesNode.getNodeType() == nAttributesNode.ELEMENT_NODE) {
                        Element eAttribut = (Element) nAttributesNode;
                        UUID idA = UUID.fromString(eAttribut
                                .getElementsByTagName("ID")
                                .item(0)
                                .getTextContent());
                        Attribute a = new Attribute(idA,
                                eAttribut
                                .getElementsByTagName("Access")
                                .item(0)
                                .getTextContent(),
                                eAttribut
                                .getElementsByTagName("Name")
                                .item(0)
                                .getTextContent(),
                                eAttribut
                                .getElementsByTagName("DataType")
                                .item(0)
                                .getTextContent());
                        i.addAttribute(a);
                    }
                }

                NodeList nListMethods = eElement.getElementsByTagName("Method");
                for (int k = 0; k < nListMethods.getLength(); k++) {
                    Node nMethodsNode = nListMethods.item(k);
                    if (nMethodsNode.getNodeType() == nMethodsNode.ELEMENT_NODE) {
                        Element eMethod = (Element) nMethodsNode;
                        UUID idM = UUID.fromString(eMethod
                                .getElementsByTagName("ID")
                                .item(0)
                                .getTextContent());
                        Method m = new Method(idM,
                                eMethod
                                .getElementsByTagName("Access")
                                .item(0)
                                .getTextContent(),
                                eMethod
                                .getElementsByTagName("Name")
                                .item(0)
                                .getTextContent(),
                                eMethod
                                .getElementsByTagName("DataType")
                                .item(0)
                                .getTextContent());
                        i.addMethod(m);
                    }
                }
                classObjects.add(i);
            }
        }

        return classObjects;
    }

    /*
     * Funkce pro nacteni Petriho siti
    */
    private ArrayList<MainObject> loadPetriNet(Element eElement, String grandgrandparent) {
        ArrayList<MainObject> pnList = new ArrayList<MainObject>();
        NodeList nListPetriNets = eElement.getElementsByTagName("PetriNet");
        if (nListPetriNets.getLength() > 0) {

            NodeList nListPlaces = ((Element) nListPetriNets.item(0)).getElementsByTagName("Place");
            for (int i = 0; i < nListPlaces.getLength(); i++) {
                Node nPlaceNode = nListPlaces.item(i);
                if (nPlaceNode.getNodeType() == nPlaceNode.ELEMENT_NODE
                        && nPlaceNode.getParentNode().getParentNode().getParentNode().getNodeName().equals(grandgrandparent)) {
                    Element ePlace = (Element) nPlaceNode;

                    String id = ePlace
                            .getElementsByTagName("ID")
                            .item(0)
                            .getTextContent();

                    Place p = new Place(UUID.fromString(id));
                    p.setX(Integer.parseInt(ePlace
                            .getElementsByTagName("X")
                            .item(0)
                            .getTextContent()));
                    p.setY(Integer.parseInt(ePlace
                            .getElementsByTagName("Y")
                            .item(0)
                            .getTextContent()));
                    p.setWidth(Integer.parseInt(ePlace
                            .getElementsByTagName("Width")
                            .item(0)
                            .getTextContent()));
                    p.setHeight(Integer.parseInt(ePlace
                            .getElementsByTagName("Height")
                            .item(0)
                            .getTextContent()));
                    p.setLabel(ePlace
                            .getElementsByTagName("Label")
                            .item(0)
                            .getTextContent());

                    String assocID = ePlace
                            .getElementsByTagName("AssocObjectId")
                            .item(0)
                            .getTextContent();
                    if (!assocID.equals("")) {
                        p.setAssocObjectID(UUID.fromString(assocID));
                    }

                    if (grandgrandparent.equals("Class")) {
                        p.setPnList(loadPetriNet(ePlace, "Place"));
                        p.setPnLineList(loadPetriNetLines(ePlace, "Place"));
                    }
                    pnList.add(p);
                }
            }

            NodeList nListTransition = ((Element) nListPetriNets.item(0)).getElementsByTagName("Transition");

            for (int i = 0; i < nListTransition.getLength(); i++) {
                Node nTransitionNode = nListTransition.item(i);
                if (nTransitionNode.getNodeType() == nTransitionNode.ELEMENT_NODE && nTransitionNode.getParentNode().getParentNode().getParentNode().getNodeName() == grandgrandparent) {
                    Element eTransition = (Element) nTransitionNode;

                    String id = eTransition
                            .getElementsByTagName("ID")
                            .item(0)
                            .getTextContent();

                    Transition t = new Transition(UUID.fromString(id));
                    t.setX(Integer.parseInt(eTransition
                            .getElementsByTagName("X")
                            .item(0)
                            .getTextContent()));
                    t.setY(Integer.parseInt(eTransition
                            .getElementsByTagName("Y")
                            .item(0)
                            .getTextContent()));
                    t.setWidth(Integer.parseInt(eTransition
                            .getElementsByTagName("Width")
                            .item(0)
                            .getTextContent()));
                    t.setHeight(Integer.parseInt(eTransition
                            .getElementsByTagName("Height")
                            .item(0)
                            .getTextContent()));
                    t.setLabel(eTransition
                            .getElementsByTagName("Label")
                            .item(0)
                            .getTextContent());
                    t.setGuard(eTransition
                            .getElementsByTagName("Guard")
                            .item(0)
                            .getTextContent());
                    t.setAction(eTransition
                            .getElementsByTagName("Action")
                            .item(0)
                            .getTextContent());

                    pnList.add(t);
                }
            }
        }

        return pnList;
    }

    /*
     * Funkce pro nacteni spojovacich hran Petriho siti
    */
    private ArrayList<Line> loadPetriNetLines(Element eElement, String grandgrandparent) {
        ArrayList<Line> pnLineList = new ArrayList<Line>();
        NodeList nListPetriNets = eElement.getElementsByTagName("PetriNet");
        if (nListPetriNets.getLength() > 0) {

            NodeList nListLines = ((Element) nListPetriNets.item(0)).getElementsByTagName("Lines");
            if (nListLines.getLength() > 0) {
                NodeList nLine = ((Element) nListLines.item(nListLines.getLength() - 1)).getElementsByTagName("Line");

                for (int temp = 0; temp < nLine.getLength(); temp++) {
                    Node nNode = nLine.item(temp);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE && nNode.getParentNode().getParentNode().getParentNode().getNodeName().equals(grandgrandparent)) {
                        Element element = (Element) nNode;

                        String id = element
                                .getElementsByTagName("ID")
                                .item(0)
                                .getTextContent();

                        Line l = new Line(UUID.fromString(id));

                        l.setStartObjectID(UUID.fromString(element
                                .getElementsByTagName("StartObjectID")
                                .item(0)
                                .getTextContent()));

                        l.setEndObjectID(UUID.fromString(element
                                .getElementsByTagName("EndObjectID")
                                .item(0)
                                .getTextContent()));

                        l.setType(element
                                .getElementsByTagName("Type")
                                .item(0)
                                .getTextContent());
                        l.setStartX(Integer.parseInt(element
                                .getElementsByTagName("StartX")
                                .item(0)
                                .getTextContent()));
                        l.setStartY(Integer.parseInt(element
                                .getElementsByTagName("StartY")
                                .item(0)
                                .getTextContent()));
                        l.setEndX(Integer.parseInt(element
                                .getElementsByTagName("EndX")
                                .item(0)
                                .getTextContent()));
                        l.setEndY(Integer.parseInt(element
                                .getElementsByTagName("EndY")
                                .item(0)
                                .getTextContent()));

                        pnLineList.add(l);
                    }
                }
            }
        }
        return pnLineList;

    }

    /*
     * Funkce pro nacteni hran class diagramu
    */
    private ArrayList<Line> loadClassDiagramLines(Document doc) {
        ArrayList<Line> lineList = new ArrayList<Line>();
        NodeList nClassDiagram = doc.getElementsByTagName("ClassDiagram");
        Node nNodeCD = nClassDiagram.item(0);
        NodeList nListUseCaseLines = ((Element) nNodeCD).getElementsByTagName("Lines");
        Node nNodeCDLines = nClassDiagram.item(0);
        NodeList nListUseLines = ((Element) nNodeCDLines).getElementsByTagName("Line");
        for (int temp = 0; temp < nListUseLines.getLength(); temp++) {
            Node nNode = nListUseLines.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE && nNode.getParentNode().getParentNode().getNodeName().equals("ClassDiagram")) {

                Element eElement = (Element) nNode;

                String id = eElement
                        .getElementsByTagName("ID")
                        .item(0)
                        .getTextContent();

                Line l = new Line(UUID.fromString(id));

                l.setStartObjectID(UUID.fromString(eElement
                        .getElementsByTagName("StartObjectID")
                        .item(0)
                        .getTextContent()));

                l.setEndObjectID(UUID.fromString(eElement
                        .getElementsByTagName("EndObjectID")
                        .item(0)
                        .getTextContent()));

                l.setType(eElement
                        .getElementsByTagName("Type")
                        .item(0)
                        .getTextContent());

                l.setStartX(Integer.parseInt(eElement
                        .getElementsByTagName("StartX")
                        .item(0)
                        .getTextContent()));
                l.setStartY(Integer.parseInt(eElement
                        .getElementsByTagName("StartY")
                        .item(0)
                        .getTextContent()));
                l.setEndX(Integer.parseInt(eElement
                        .getElementsByTagName("EndX")
                        .item(0)
                        .getTextContent()));
                l.setEndY(Integer.parseInt(eElement
                        .getElementsByTagName("EndY")
                        .item(0)
                        .getTextContent()));

                lineList.add(l);
            }
        }

        return lineList;
    }
}
