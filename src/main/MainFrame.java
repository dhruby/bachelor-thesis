/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package main;

import MenuBar.MenuBar;
import TabbedPanel.TabbedPanel;
import ToolBar.ToolBar;
import java.awt.HeadlessException;
import javax.swing.JFrame;


/*
 * Trida hlavniho framu
*/
public class MainFrame extends JFrame {

    private TabbedPanel tabbedPanel;
    private MenuBar menuBar;

    /*
     * Konstruktor s nastavenim nazvu
    */
    public MainFrame(String title) throws HeadlessException {
        super(title);
    }

    /*
     * Inicializace zakladnich komponent
    */
    protected void init() {
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(1024, 600));
        setResizable(true);

        tabbedPanel = new TabbedPanel();
        menuBar = new MenuBar(tabbedPanel);

        //setLocationRelativeTo(null);
        setJMenuBar(menuBar);

        // add(toolbar, BorderLayout.EAST);
        add(tabbedPanel);

        pack();
        setVisible(true);
    }
}
