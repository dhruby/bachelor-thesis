/* 
 * Predmet:     Bakalarska prace
 * Projekt:     Nastroj pro podporu vyvoje softwarovych systemu
 * Autor:       Daniel Hruby
 * Login:       xhruby21
 * Email:       xhruby21@stud.fit.vutbr.cz
 */
package main;

/*
 * Trida pro spusteni hlavniho framu
*/
public class MainApp {
    public static void main(String[] args) {  
        MainFrame frame = new MainFrame("Bachelor thesis");
        frame.init();
    }
}
